import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
import cPickle
import argparse
import os
import numpy as np

import _init_paths
import misc
from tless.evaluation import load_gt, compute_iou
from frcnn.plot_bboxhist import get_name_list

misc.latexize_matplotlib()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('root')
    parser.add_argument('for_each', choices=['gt', 'proposal'])
    return parser.parse_args()


def load_rpn(root):
    with open(os.path.join(root, 'rpn.pkl')) as f:
        return cPickle.load(f)


def unique_argmax(mat):
    """
    Indices of maximums in *mat*, so that there is only one in each row and column
    :param mat: 2D matrix
    :return: indices of rows and columns
    """
    assert len(mat.shape) == 2
    mat = mat.astype(np.float, copy=True)
    rs = []
    cs = []
    while np.sum(np.isnan(mat)) < np.prod(mat.shape):
        # while there is some non-nan value
        r, c = np.unravel_index(np.nanargmax(mat), mat.shape)
        rs.append(r)
        cs.append(c)
        mat[r, :] = np.nan
        mat[:, c] = np.nan
    return rs, cs


def get_iou_oness(rpn, gt, method):
    iou = []
    oness = []
    for i_img in sorted(rpn.keys()):
        # if i_img > 100:
        #     break
        print(i_img)
        iou_img = compute_iou(gt[i_img]['bbox'], rpn[i_img]['bbox'])
        if method == 'gt':
            # find the best proposal for each gt
            # r, c = unique_argmax(iou_img)  # unnecessary and possibly bad
            c = np.argmax(iou_img, axis=1)
            oness.extend(rpn[i_img]['score'][c].flatten().tolist())
            iou.extend(iou_img[np.arange(len(iou_img)), c].flatten().tolist())
        elif method == 'proposal':
            # consider all proposals which means many gt duplicates (300 proposals vs ~10 gt)
            oness.extend(rpn[i_img]['score'].flatten().tolist())
            iou.extend(iou_img.max(axis=0).tolist())
    return iou, oness


def plot_iou_vs_objectness(root, method):
    gt = load_gt(get_name_list(misc.split_path(root)[-1]))
    rpn = load_rpn(root)
    iou, oness = get_iou_oness(rpn, gt, method)

    pol = np.poly1d(np.polyfit(iou, oness, 1))
    x = np.linspace(0, 1, 2)
    y = pol(x)

    hist, _, _, _ = plt.hist2d(iou, oness, bins=20, norm=LogNorm(), range=((0, 1), (0, 1)))
    fig = plt.figure(figsize=(4, 4), dpi=300)
    ax = fig.add_subplot(1, 1, 1)
    im = ax.imshow(hist.T, extent=[0, 1, 0, 1], interpolation='nearest', norm=LogNorm(), origin='lower')
    # ax.plot(x, y, linestyle=':', marker='')
    fig.colorbar(im, cax=make_axes_locatable(ax).append_axes("right", size="5%", pad=0.1))
    ax.set_xlabel('IoU')
    ax.set_ylabel('objectness')
    ax.set_xlim((0, 1))
    ax.set_ylim((0, 1))
    ax.grid(True, alpha=0.2)
    fig.savefig(os.path.join(root, 'hist_iou_objectness_' + method + '.pdf'), bbox_inches='tight')

    fig = plt.figure(figsize=(5, 5), dpi=300)
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(iou, oness, linestyle='', marker='1', markersize=0.3)
    # ax.plot(x, y, linestyle=':', marker='')
    ax.set_xlabel('IoU')
    ax.set_ylabel('objectness')
    ax.set_xlim((0, 1))
    ax.set_ylim((0, 1))
    ax.grid(True, alpha=0.2)
    fig.savefig(os.path.join(root, 'plot_iou_objectness_' + method + '.png'), bbox_inches='tight')


def main():
    args = parse_args()
    print('Using ' + str(args))
    plot_iou_vs_objectness(args.root, args.for_each)


if __name__ == '__main__':
    main()
