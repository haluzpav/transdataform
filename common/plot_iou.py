import matplotlib.pyplot as plt
import numpy as np

import _init_paths
import misc

misc.latexize_matplotlib()

fig = plt.figure(figsize=(4, 4), dpi=300)
ax = fig.add_subplot(1, 1, 1)

x = np.linspace(0, 1, 200, dtype=float)

ax.plot(x, 1 - x, '--', label='linear (reference)', alpha=0.5)

i = (1 - x)
u = 2 - i
iou = i / u
ax.plot(x, iou, label='shift in one axis')

i = (1 - x) ** 2
u = 2 - i
iou = i / u
ax.plot(x, iou, label='shift in both axes')

ax.set_xlabel('shift in terms of square width')
ax.set_ylabel('IoU')
ax.set_xlim((0, 1))
ax.set_ylim((0, 1))
ax.grid(True, alpha=0.2)
ax.legend()

fig.savefig(r'iou.png', bbox_inches='tight')
