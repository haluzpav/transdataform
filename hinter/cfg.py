import os

root_root = '/datagrid/personal/haluzpav/hinter'
root = root_root
root_lists = os.path.join(root, 'lists')
img_sizes = {
    'train': (640, 480),
    'test': (640, 480),
}
n_cls = 15
has_cutable_elevs = False
