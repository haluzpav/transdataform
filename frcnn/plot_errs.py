import pandas as pd
import numpy as np
import sys
import os

import _init_paths
import common


def plot_errs(root):
    evals = common.load_evals(root)
    data = np.zeros((30, 6), float)
    for i_cls in range(1, 31):
        is_cls = evals['clss'] == i_cls
        count_cls = np.sum(is_cls)
        data[i_cls - 1, 0] = count_cls
        if count_cls == 0:
            data[i_cls - 1, 1:] = np.nan
            continue
        for i_err in range(5):
            count_err = np.sum(np.logical_and(is_cls, evals['errs'] == i_err))
            data[i_cls - 1, i_err + 1] = 100.0 * count_err / count_cls
    df = pd.DataFrame(data, list(range(1, 31)), ['count', 'tp', 'loc', 'sim', 'oth', 'bg'])
    print('')
    with pd.option_context("display.float_format", '{:.0f}'.format):
        print(df)
    print('')
    print('confusion matrix (row = one gt class, column = one detected class)')
    with pd.option_context("display.float_format", '{:.0f}'.format, 'display.max_columns', 50, 'display.width', 1000):
        print(pd.DataFrame(evals['conmat'], list(range(0, 31)), list(range(1, 31))))


if __name__ == '__main__':
    plot_errs(sys.argv[1])
