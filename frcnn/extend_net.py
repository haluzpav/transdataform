import sys
import numpy as np

from _init_paths import frcnn_paths

frcnn_paths()

import caffe


def load_net(path_prototxt, path_caffemodel=None):
    if path_caffemodel is None:
        return caffe.Net(path_prototxt, caffe.TEST)
    else:
        return caffe.Net(path_prototxt, path_caffemodel, caffe.TEST)


def copy_weights(net_src, net_dst):
    assert list(net_src.params) == list(net_dst.params), 'nets are not same enough'
    for ((name_layer, params_src), (_, params_dst)) in zip(net_src.params.items(), net_dst.params.items()):
        print('copying layer \'{}\''.format(name_layer))
        unwrap = lambda p: [p[i].data for i in range(2)]
        weights_src, biases_src = unwrap(params_src)
        weights_dst, biases_dst = unwrap(params_dst)
        if name_layer == 'conv1':
            weights_dst[:, :3, ...] = weights_src
        else:
            weights_dst[:] = weights_src
        biases_dst[:] = biases_src


def init_depth(net, method):
    w = net.params['conv1'][0].data
    w_depth_shape = list(w.shape)
    w_depth_shape[1] -= 3

    if method == 'copy':
        w_rgb = w[:, :3, ...]
        w[:] = np.pad(w_rgb, ((0, 0), (0, w_depth_shape[1]), (0, 0), (0, 0)), 'wrap')
        return

    if method == 'zeros':
        w_depth = np.zeros(w_depth_shape)
    elif method == 'normal':
        n_inputs = np.prod(w_depth_shape)
        w_depth = np.random.randn(*w_depth_shape) / np.sqrt(n_inputs)
    else:
        raise NotImplementedError
    w[:, 3:, ...] = w_depth


def main(method, path_prototxt_old, path_prototxt_new, path_caffemodel_old, path_caffemodel_new):
    print('loading')
    net_old = load_net(path_prototxt_old, path_caffemodel_old)
    net_new = load_net(path_prototxt_new)
    copy_weights(net_old, net_new)
    init_depth(net_new, method)
    print('saving')
    net_new.save(path_caffemodel_new)


if __name__ == '__main__':
    main(*sys.argv[1:])
