import os
import sys

import matplotlib.pyplot as plt
import numpy as np

import _init_paths
import misc
import common


def plot_hist(root):
    data = common.load_evals(root)
    fig = plt.figure(figsize=(6, 5), dpi=100)
    ax = fig.add_subplot(1, 1, 1)
    tps = data['errs'] == 0
    ax.hist((
        data['scores'][tps],
        data['scores'][np.logical_not(tps)]
    ), bins=10, range=(0, 1), color=('green', 'red'))
    ax.plot(np.nanmean(data['recall_cls'], 0), np.nanmean(data['precision_cls'], 0), label='mean')
    ax.set_title(os.path.join(*misc.split_path(root)[-2:]))
    ax.set_xlabel('score')
    ax.set_ylabel('count')
    fig.savefig(os.path.join(root, 'plot_scores.png'), bbox_inches='tight')


if __name__ == '__main__':
    plot_hist(sys.argv[1])
