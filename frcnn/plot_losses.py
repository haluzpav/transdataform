import os.path as osp
import re
import sys
import argparse

import matplotlib.pyplot as plt
import numpy as np

import _init_paths

import misc

misc.latexize_matplotlib()

from frcnn.plot_pr import detokenize


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('paths', nargs='+')
    parser.add_argument('--path_overall')
    parser.add_argument('--labels_overall', nargs='+')
    args = parser.parse_args()
    return args


def parse_log(path):
    data_raw = {}
    pattern_start = r'solver\.cpp.*'
    pattern_main = pattern_start + r'Iteration ([^\s]+), loss = ([^\s]+)'
    pattern_detail = pattern_start + r'Train net output #[0-9]: ([^\s]+) = ([^\s]+)'
    with open(path) as log:
        for line in log:
            try:
                iter_n, loss = re.search(pattern_main, line).groups()
                iter_n, loss = int(iter_n), float(loss)
                misc.append_dict(data_raw, 'iters', iter_n)
                misc.append_dict(data_raw, 'loss', loss)
                continue
            except AttributeError:
                pass
            try:
                name, loss = re.search(pattern_detail, line).groups()
                loss = float(loss)
                misc.append_dict(data_raw, name, loss)
                continue
            except AttributeError:
                pass
    data = {
        'iters': data_raw.pop('iters', None),
        'labels': [],
        'values': np.zeros((len(data_raw.values()[0]), len(data_raw)), float),
    }
    for i, name in enumerate(data_raw):
        data['labels'].append(detokenize(name))
        data['values'][:, i] = data_raw[name]
    return data


def plot_losses(path, data):
    fig = plt.figure(figsize=(6, 5), dpi=100)
    ax = fig.add_subplot(1, 1, 1)
    sigma = 25  # just looks great
    data_smooth = misc.moving_average(data['values'], sigma, 'gauss')
    lines = ax.semilogy(data['iters'], data_smooth)
    ax.legend(lines, data['labels'], loc='lower left')
    ax.set_xlim(0, max(data['iters']))
    ax.grid()

    path_split = misc.split_path(path)
    file_name = osp.splitext(path_split[-1])[0]
    path_fig = osp.join(*(path_split[:-1] + [file_name + '.png']))
    fig.savefig(path_fig, bbox_inches='tight')


def plot_losses_overall(datas, path_dst, labels):
    fig = plt.figure(figsize=(5, 4), dpi=300)
    ax = fig.add_subplot(1, 1, 1)
    sigma = 25  # just looks great
    for i, d in enumerate(datas):
        data_smooth = misc.moving_average(d['values'][:, 0], sigma, 'gauss')
        ax.semilogy(d['iters'], data_smooth, label=(d['labels'][0] if labels is None else labels[i]))
    ax.legend(loc='center right')
    # ax.set_xlim(0, max(max(d['iters']) for d in datas))
    ax.set_xlim(0, 1e5)
    ax.set_ylim(ymin=1e-1)
    ax.set_xlabel('iterations')
    ax.set_ylabel('loss')
    ax.grid(True, alpha=0.2)
    fig.savefig(path_dst, bbox_inches='tight')


def main():
    args = parse_args()
    datas = [parse_log(path) for path in args.paths]
    for p, d in zip(args.paths, datas):
        plot_losses(p, d)
    if args.path_overall is not None:
        plot_losses_overall(datas, args.path_overall, args.labels_overall)


if __name__ == '__main__':
    main()
