import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import AxesGrid
import numpy as np
import os
import argparse

import _init_paths
from extend_net import load_net

_init_paths.frcnn_paths()

import caffe


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('prototxt')
    parser.add_argument('caffemodel')
    parser.add_argument('layer', type=int)
    return parser.parse_args()


def get_int_ratio(n, max_width, row_divider=1):
    for c in range(max_width, 0, -1):
        if n % c == 0 and int(n / c) % row_divider == 0:
            return int(n / c), c


def main():
    args = parse_args()
    net = load_net(args.prototxt, args.caffemodel)
    names_layer = sorted(net.params.keys())
    weights = [net.params[name][0].data for name in names_layer if name.startswith('conv')]
    has_rgb = 'rgb' in args.caffemodel

    print('starting plotting')
    fig = plt.figure(figsize=(8, 8), dpi=300)
    ax = fig.add_subplot(1, 1, 1)
    w = weights[args.layer]
    n_filt, n_chan, h_filt, w_filt = w.shape
    n_r, n_c = 3, n_filt / 3
    h_space = int(h_filt / 2)
    assert n_r * n_c == n_filt
    n_chan_vis = n_chan - (2 if has_rgb else 0)
    has_spaces = n_chan_vis > 1
    img = np.zeros((n_r * n_chan_vis * h_filt + ((n_r - 1) * h_space if has_spaces else 0), n_c * w_filt, 3),
                   np.uint8) + 255
    yticks = []
    for r_grid in range(n_r):
        for c_grid in range(n_c):
            i_grid = r_grid * n_c + c_grid
            for i_chan in range(n_chan_vis):
                t = (r_grid * n_chan_vis + i_chan) * h_filt + (r_grid * h_space if has_spaces else 0)
                l = c_grid * w_filt
                if t not in yticks:
                    yticks.append(t)
                if t + h_filt not in yticks:
                    yticks.append(t + h_filt)
                if i_chan == 0 and has_rgb:
                    filt = w[i_grid, 2::-1, ...]
                    filt = filt.transpose((1, 2, 0))
                else:
                    filt = w[i_grid, i_chan - (2 if has_rgb else 0), ...]
                    filt = np.tile(filt[..., np.newaxis], (1, 1, 3))
                filt -= filt.min()
                filt = (filt * 255 / filt.max()).astype(np.uint8)
                img[t:t + h_filt, l:l + w_filt, :] = filt
                # i_channel_filt = r_grid % len(args.channels)
                # i_filt = c_grid + n_c * int(r_grid / len(args.channels))
                # filt = w[i_filt, args.channels[i_channel_filt], ...]
                # img[r_grid * w.shape[2]:(r_grid + 1) * w.shape[2], c_grid * w.shape[3]:(c_grid + 1) * w.shape[3]] = filt

    vmin, vmax = img.min(), img.max()  # same color scale for all
    im = ax.imshow(img, interpolation='nearest')  # , vmin=vmin, vmax=vmax
    ax.xaxis.set_ticks(np.arange(0, n_c + 1, 1) * w.shape[3] - 0.5)
    ax.yaxis.set_ticks(np.array(yticks) - 0.5)
    ax.xaxis.set_ticks_position('none')
    ax.yaxis.set_ticks_position('none')
    # ax.tick_params(top='off', bottom='off', left='off', right='off', labelleft='off', labelbottom='off')
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.grid(True, alpha=1)
    # fig.colorbar(im, ax=ax)

    # grid = AxesGrid(fig, '111',
    #                 nrows_ncols=(n_r, n_c),
    #                 axes_pad=0.01,
    #                 share_all=True,
    #                 label_mode='1',
    #                 cbar_mode='single',
    #                 )
    # print('plotting')
    # vmin, vmax = w.min(), w.max()  # same color scale for all
    # for i_grid in range(w.shape[0] * len(args.channels)):
    #     print(i_grid)
    #     r_grid = int(i_grid / n_c)
    #     c_grid = i_grid % n_c
    #     i_channel_filt = r_grid % len(args.channels)
    #     i_filt = c_grid + n_c * int(r_grid / len(args.channels))
    #     filt = w[i_filt, args.channels[i_channel_filt], ...]
    #     im = grid[i_grid].imshow(filt, vmin=vmin, vmax=vmax, interpolation='nearest')
    #     grid[i_grid].axis('off')
    # grid.cbar_axes[0].colorbar(im)

    print('saving plot')
    root, name = os.path.split(args.caffemodel)
    fig.savefig(os.path.join(root, 'plot_weights_' + os.path.splitext(name)[0] + '.pdf'), bbox_inches='tight')


if __name__ == '__main__':
    main()
