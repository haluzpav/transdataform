import os
import argparse
import matplotlib.pyplot as plt

import _init_paths
import misc

misc.latexize_matplotlib()


def parse_args(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('dst_summary')
    parser.add_argument('src_roots', nargs='+')
    parser.add_argument('--groups', nargs='+')
    parser.add_argument('--grouping', default='count')
    parser.add_argument('--labels', nargs='+')
    parser.add_argument('--legend_outside', action='store_true')
    args = parser.parse_args(args)
    args.groups = parse_groups(len(args.src_roots), args.groups, args.grouping)
    if args.labels is not None:
        assert len(args.src_roots) == len(args.labels)
    return args


def parse_groups(len_roots, groups_in, grouping):
    if groups_in is None:
        groups = [[i] for i in range(len_roots)]
    elif groups_in[0] == 'opposite':
        groups = []
        for i in range(int(len_roots / 2.0 + 0.6)):
            ii = len_roots - 1 - i
            groups.append([i] if i == ii else [i, ii])
    else:
        # numbers of items in each group, e.g. (2, 1, 3) -> [[0, 1], [2], [3, 4, 5]]
        groups_in = [int(g) for g in groups_in]
        if grouping == 'count':
            assert len_roots == sum(groups_in)
            groups = []
            cumsum = 0
            for g in groups_in:
                groups.append(list(range(cumsum, cumsum + g)))
                cumsum += g
        elif grouping == 'number':
            assert len_roots == len(groups_in)
            dict_groups = {}
            for i, g in enumerate(groups_in):
                if g in dict_groups:
                    dict_groups[g].append(i)
                else:
                    dict_groups[g] = [i]
            groups = []
            for k in sorted(dict_groups.keys()):
                groups.append(dict_groups[k])

    # basic sanity check
    items = []
    for g in groups:
        if len(groups) > 10:
            assert len(g) == 1
        else:
            assert len(g) <= 4
        for i in g:
            assert i not in items
            items.append(i)

    return groups


def degroup(groups):
    colors_default = plt.rcParams['axes.prop_cycle'].by_key()['color']
    linestyles_default = misc.styles_line
    len_plots = sum([len(g) for g in groups])
    colors = [None for _ in range(len_plots)]
    linestyles = [None for _ in range(len_plots)]
    for ig, g in enumerate(groups):
        for ii, i in enumerate(g):
            colors[i] = colors_default[ig % len(colors_default)]
            linestyles[i] = linestyles_default[ii]
    return colors, linestyles


def plot_single(path_dst, data):
    fig = plt.figure(figsize=(10, 9), dpi=300)
    ax = fig.add_subplot(1, 1, 1)
    r, p = data['recall'], data['precision']
    ro, po = data['recall_overall'], data['precision_overall']
    for i in range(r.shape[0]):
        ax.plot(r[i], p[i], label=i + 1, linestyle=misc.get_style_line(i))
    # ax.plot(np.nanmean(r, 0), np.nanmean(p, 0), label='mean', linestyle=':')
    ax.plot(ro, po, label='overall', linestyle=':')
    ax.legend(ncol=1, loc='center left', bbox_to_anchor=(1, 0.5))
    ax.set_title(detokenize(os.path.join(*misc.split_path(path_dst)[-3:-1])))
    ax.set_xlabel('recall')
    ax.set_ylabel('precision')
    ax.yaxis.get_major_formatter().set_useOffset(False)
    fig.savefig(path_dst, bbox_inches='tight')
    print('Plotted ' + path_dst)


def plot_summary(path_dst, datas, labels, groups, legend_outside):
    cs, ls = degroup(groups)
    fig = plt.figure(figsize=(4, 4), dpi=300)
    ax = fig.add_subplot(1, 1, 1)
    for i, data in enumerate(datas):
        r, p = data['recall'], data['precision']
        ro, po = data['recall_overall'], data['precision_overall']
        ax.plot(ro, po, label=labels[i], color=cs[i], linestyle=ls[i])
    if legend_outside:
        ax.legend(loc='center left', bbox_to_anchor=(1.05, 0.5))
    else:
        ax.legend(loc='lower left')
    # ax.set_title(misc.split_path(path_dst)[-1])
    ax.set_xlabel('recall')
    ax.set_ylabel('precision')
    ax.set_xlim((0, 1))
    ax.set_ylim((0, 1))
    ax.grid(True, alpha=0.2)
    ax.yaxis.get_major_formatter().set_useOffset(False)
    fig.savefig(path_dst, bbox_inches='tight')
    print('Plotted ' + path_dst)


def detokenize(s):
    return '\\detokenize{' + s + '}'


def plot_all(dst_summary, src_roots, groups, labels, legend_outside):
    print('Starting plotting recall-precision curve')

    print('Loading evaluations')
    datas = [misc.load_evals(root) for root in src_roots]
    if labels is None:
        labels = [os.path.join(*misc.split_path(root)[-3:]) for root in src_roots]
    # labels = [detokenize(l) for l in labels]

    print('Plotting')
    if len(datas) > 1:
        plot_summary(dst_summary, datas, labels, groups, legend_outside)
    for root, data in zip(src_roots, datas):
        plot_single(os.path.join(root, 'plot_pr.png'), data)


def main():
    args = parse_args()
    plot_all(args.dst_summary, args.src_roots, args.groups, args.labels, args.legend_outside)


if __name__ == '__main__':
    main()
