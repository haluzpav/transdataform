import os
import argparse
import cv2
import numpy as np

import _init_paths
from tless.evaluation import draw_rect
from common.plot_rpn import load_rpn
from frcnn.plot_bboxhist import get_name_list
import tless.cfg as cfg


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('root')
    parser.add_argument('--ids_img', type=int, nargs='+')
    parser.add_argument('--thresh_score', type=float, default=0)
    args = parser.parse_args()
    return args


def save_img(img, path):
    save_root = os.path.split(path)[0]
    if not os.path.exists(save_root):
        os.makedirs(save_root)
    cv2.imwrite(path, img)


def draw_rpn_bboxes(img, bboxs, scores):
    color_red = np.array((0, 0, 255), float)
    for bb, score in zip(bboxs, scores):
        assert 0 <= score <= 1
        color = (color_red * score).astype(int).tolist()
        draw_rect(img, bb.astype(int), color, 1)


def main():
    args = parse_args()
    print('using ' + str(args))

    rpn = load_rpn(args.root)
    with open(os.path.join(cfg.root_lists, get_name_list(args.root) + '.txt')) as f:
        for i_img, path_img in enumerate(f):
            path_img = path_img.strip()
            # i_img_real = int(os.path.splitext(os.path.split(path_img)[-1])[0])
            # print(i_img, i_img_real)
            if args.ids_img is not None and i_img not in args.ids_img:
                continue
            img = cv2.imread(path_img)
            bbox = rpn[i_img]['bbox']
            score = rpn[i_img]['score'].flatten()
            select = score >= args.thresh_score
            draw_rpn_bboxes(img, bbox[select], score[select])
            save_img(img, os.path.join(args.root, 'vis_rpn', '{:04d}.png'.format(i_img)))


if __name__ == '__main__':
    main()
