import matplotlib.pyplot as plt
import os
import sys
import numpy as np

import _init_paths
from tless.evaluation import load_det, load_gt
import misc


def get_bbox_i_from_gt(i, gt):
    return np.array([bb[i] for img in gt for bb in img['bbox']])


def get_bbox_i_from_det(i, det):
    return np.array([bb[i] for i_img in det for bb in det[i_img]['bbox']])


def get_bbox_i_from_eval(i, ev):
    i_tp = ev['err'] == 0
    return ev['bbox'][i_tp, i]


def get_name_list(root):
    raw = os.path.split(root)[-1]
    i_end = raw.find('_iter')
    if i_end < 0:
        return raw
    else:
        return raw[:i_end]


def plot_hists_det(root_det):
    name_list = get_name_list(root_det)
    gt = load_gt(name_list, list(range(1, 31 if name_list.startswith('train') else 21)))
    det = load_det(root_det)

    fig = plt.figure(figsize=(10, 6), dpi=300)
    ax = fig.add_subplot(1, 1, 1)

    for i, name in enumerate(('xleft', 'ytop', 'xright', 'ybottom', 'width', 'height')):
        print('Plotting histogram of ' + name)

        if i < 4:
            g = get_bbox_i_from_gt(i, gt)
            d = get_bbox_i_from_det(i, det)
        elif i < 6:
            g = get_bbox_i_from_gt(i - 2, gt) - get_bbox_i_from_gt(i - 4, gt)
            d = get_bbox_i_from_det(i - 2, det) - get_bbox_i_from_det(i - 4, det)
        else:
            raise NotImplementedError

        plot_single(ax, g, d, ('gt', 'det'), True, 20)
        ax.legend()
        ax.set_title(os.path.join(*misc.split_path(root_det)[-3:]))
        ax.set_xlabel(name)
        fig.savefig(os.path.join(root_det, '_'.join(('plot', 'hist', 'det', name)) + '.png'), bbox_inches='tight')


def plot_single(ax, d1, d2, labels, normed=False, bins=20):
    plt.cla()
    range_hist = (
        min(d1.min(), d2.min()),
        max(d1.max(), d2.max())
    )
    ax.hist(d1, label=labels[0], alpha=0.5, normed=normed, bins=bins, range=range_hist)
    ax.hist(d2, label=labels[1], alpha=0.5, normed=normed, bins=bins, range=range_hist)


def plot_hists_eval(root_eval):
    name_list = get_name_list(root_eval)
    gt = load_gt(name_list, list(range(1, 31 if name_list.startswith('train') else 21)))
    ev = misc.load_evals(root_eval)

    fig = plt.figure(figsize=(10, 6), dpi=300)
    ax = fig.add_subplot(1, 1, 1)

    for i, name in enumerate(('xleft', 'ytop', 'xright', 'ybottom', 'width', 'height')):
        print('Plotting histogram of ' + name)

        if i < 4:
            g = get_bbox_i_from_gt(i, gt)
            e = get_bbox_i_from_eval(i, ev)
        elif i < 6:
            g = get_bbox_i_from_gt(i - 2, gt) - get_bbox_i_from_gt(i - 4, gt)
            e = get_bbox_i_from_eval(i - 2, ev) - get_bbox_i_from_eval(i - 4, ev)
        else:
            raise NotImplementedError
        plot_single(ax, g, e, ('gt', 'eval'), False, 50)
        ax.legend()
        ax.set_title(os.path.join(*misc.split_path(root_eval)[-3:]))
        ax.set_xlabel(name)
        fig.savefig(os.path.join(root_eval, '_'.join(('plot', 'hist', 'eval', name)) + '.png'), bbox_inches='tight')


def main(compare_to, *args):
    for r in args:
        if compare_to == 'det':
            plot_hists_det(r)
        elif compare_to == 'eval':
            plot_hists_eval(r)
        else:
            raise NotImplementedError('undefined compare target ' + compare_to)


if __name__ == '__main__':
    main(*sys.argv[1:])
