import os.path as osp
import sys


def add_path(path):
    if path not in sys.path:
        sys.path.insert(0, path)


this_dir = osp.dirname(__file__)

add_path(osp.join(this_dir, '..'))


def frcnn_paths():
    add_path(r'/mnt/home.stud/haluzpav/frcnn/caffe-fast-rcnn/python')
    add_path(r'/mnt/home.stud/haluzpav/frcnn/lib')
