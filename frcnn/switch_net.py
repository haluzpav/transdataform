import argparse
import numpy as np

from _init_paths import frcnn_paths
from frcnn.extend_net import load_net

frcnn_paths()

import caffe


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('method')
    parser.add_argument('path_prototxt_old')
    parser.add_argument('path_prototxt_new')
    parser.add_argument('path_caffemodel_old')
    parser.add_argument('path_caffemodel_new')
    parser.add_argument('layers')
    return parser.parse_args()


def copy_weights(net_src, net_dst):
    assert list(net_src.params) == list(net_dst.params), 'nets are not same enough'
    for ((name_layer, params_src), (_, params_dst)) in zip(net_src.params.items(), net_dst.params.items()):
        print('copying layer \'{}\''.format(name_layer))
        unwrap = lambda p: [p[i].data for i in range(2)]
        weights_src, biases_src = unwrap(params_src)
        weights_dst, biases_dst = unwrap(params_dst)
        n_copyable = min(weights_src.shape[1], weights_dst.shape[1])
        weights_dst[:, :n_copyable, ...] = weights_src[:, :n_copyable, ...]
        biases_dst[:] = biases_src


def init_weights_layer(net, method, name_layer):
    print('initing layer \'{}\''.format(name_layer))

    w = net.params[name_layer][0].data
    b = net.params[name_layer][1].data

    if method == 'zeros':
        # hey, that's easy!
        return
    elif method == 'normal':
        n_inputs = np.prod(w.shape)
        w[:] = np.random.randn(*w.shape) / np.sqrt(n_inputs)
    else:
        raise NotImplementedError

    b[:] = 0


def init_depth_as_rgb_mean(net_src, net_dst):
    ws = net_src.params['conv1'][0].data
    bs = net_src.params['conv1'][1].data
    wd = net_dst.params['conv1'][0].data
    bd = net_dst.params['conv1'][1].data
    assert wd.shape[1] == 1 or wd.shape[1] == 4
    wd[:, -1, ...] = ws.mean(axis=1)
    bd[:] = 0


def main():
    args = parse_args()
    print('using ' + str(args))

    print('loading')
    net_old = load_net(args.path_prototxt_old, args.path_caffemodel_old)
    net_new = load_net(args.path_prototxt_new)

    copy_weights(net_old, net_new)

    if args.layers == 'first':
        if args.method == 'rgb_mean':
            init_depth_as_rgb_mean(net_old, net_new)
        else:
            init_weights_layer(net_new, args.method, 'conv1')
    elif args.layers == 'convs':
        for name_layer in net_new.params.keys():
            if name_layer.startswith('conv'):
                init_weights_layer(net_new, args.method, name_layer)
    elif args.layers == 'all':
        for name_layer in net_new.params.keys():
            init_weights_layer(net_new, args.method, name_layer)
    else:
        raise NotImplementedError

    print('saving')
    net_new.save(args.path_caffemodel_new)


if __name__ == '__main__':
    main()
