import sys
import os
import shutil
import glob


root_darknet_data = os.path.expanduser('~/data/darknet/')
path_log = os.path.join(root_darknet_data, 'log.csv')
path_vis = os.path.join(root_darknet_data, 'vis')
path_backup = os.path.join(root_darknet_data, 'backup')
weights_keep = [100, 500, 1000] + list(range(5000, 100000, 5000))


def main(args):
    last_exp_path = sorted(glob.glob(os.path.join(root_darknet_data, 'trained', '*')))[-1]
    last_exp_id = int(os.path.split(last_exp_path)[-1].split('_')[0])
    exp_path = os.path.join(root_darknet_data, 'trained', '{:02d}_{}'.format(last_exp_id + 1, args[1]), '')
    os.mkdir(exp_path, 0o755)

    shutil.copy(path_log, exp_path)
    if os.listdir(path_vis):
        shutil.move(path_vis, exp_path)
        os.mkdir(path_vis, 0o755)
    if len(args) > 2 and os.path.exists(args[2]):
        shutil.copy2(args[2], exp_path)

    max_i = 0
    max_wp = ''
    for wp in sorted(glob.glob(os.path.join(path_backup, '*.weights')), reverse=True):
        i_str = wp.split('.')[-2].split('_')[-1]
        if i_str == 'final':
            max_i = None
        try:
            i = int(i_str)
        except ValueError:
            i = None
        if max_i is not None and i > max_i:
            max_i = i
            max_wp = wp
        if i is not None and i not in weights_keep:
            continue
        shutil.move(wp, exp_path)
    if max_i and max_i not in weights_keep and max_wp:
        shutil.move(max_wp, exp_path)
    shutil.rmtree(path_backup)
    os.mkdir(path_backup, 0o755)


if __name__ == '__main__':
    sys.exit(main(sys.argv))
