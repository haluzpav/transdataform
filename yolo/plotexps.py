import os
import sys

import matplotlib.pyplot as plt
import numpy as np

import misc

root_exps = r'D:\CMP\bak\trained'


def plotexps(ids):
    plt.figure(1)
    for d in os.listdir(root_exps):
        try:
            i = int(d.split('_')[0])
        except ValueError:
            continue
        if ids is not None and i not in ids:
            continue
        log_path = os.path.join(root_exps, d, 'log.csv')
        if not os.path.exists(log_path):
            continue
        losses = np.loadtxt(log_path, delimiter=',', usecols=[1])
        plt.semilogy(misc.moving_average_1d(losses, 50), label=d)
    plt.legend()
    plt.grid()
    plt.show()


def main(args):
    ids = list(map(int, args[1:])) if len(args) > 1 else None
    plotexps(ids)


if __name__ == '__main__':
    sys.exit(main(sys.argv))
