import numpy as np
import sys
from glob import glob
import os
import yaml

import _init_paths
import cfg
import misc


def compare_bboxes(dir_name_1, dir_name_2):
    def convert_bb(bb):
        bb[2] += bb[0]
        bb[3] += bb[1]
        return np.array([bb])

    paths_1 = sorted(glob(os.path.join(cfg.root, dir_name_1, '*', 'gt.yml')))
    paths_2 = sorted(glob(os.path.join(cfg.root, dir_name_2, '*', 'gt.yml')))
    ious = []
    for i_path, (p1, p2) in enumerate(zip(paths_1, paths_2)):
        with open(p1) as f:
            d1 = yaml.load(f, Loader=yaml.CLoader)
        with open(p2) as f:
            d2 = yaml.load(f, Loader=yaml.CLoader)
        for i_img in d1:
            bb1 = convert_bb(d1[i_img][0]['obj_bb'])
            bb2 = convert_bb(d2[i_img][0]['obj_bb'])
            ious.append(misc.compute_iou(bb1, bb2)[0, 0])
    print('min: {}\nmax: {}\navg: {}'.format(min(ious), max(ious), np.mean(ious)))
    print('worst 100:')
    i_sort = np.argsort(ious)
    for i in range(100):
        print('id_img: {}, iou: {}'.format(i_sort[i], ious[i_sort[i]]))


if __name__ == '__main__':
    compare_bboxes(*sys.argv[1:3])
