import numpy as np
import warnings

warnings.simplefilter('always', UserWarning)


def _warning(message, category, filename, lineno, file=None, line=None):
    if category == UserWarning:
        # prettify my warnings
        print('warning: ' + str(message))
    else:
        warnings.showwarning(message, category, filename, lineno, file, line)


warnings.showwarning = _warning


def median_skimage(img):
    """
    Iteratively fills holes (=0) with median filter. Slow as duck, especially for 16bit (for some weird reason).
    3 s/img. Yes, SECONDS.
    """
    from skimage import filters, morphology
    mask = img == 0
    img = img.copy()
    while np.sum(mask) > 0:
        img_median = filters.median(img, selem=morphology.square(3), mask=mask == 0)
        mask_new = img_median == 0
        i_change = np.where(np.logical_and(mask, mask_new == 0))
        img[i_change] = img_median[i_change]
        mask = mask_new
    return img


def median_generic(img):
    from scipy.ndimage.filters import generic_filter
    def nonzeromedian(values):
        center = values[4]
        if center != 0:
            return center
        values = values[values > 0]
        return np.median(values) if len(values) > 0 else 0

    while np.sum(img == 0):
        img = generic_filter(img, nonzeromedian, (3, 3), mode='constant')
    return img


def median(img):
    """
    Iteratively fills holes (=0) with median filter. Inefficient for iterating in Python,
    but still significantly faster (for small holes). 90 ms/img.
    """
    assert len(img.shape) == 2
    mask = img == 0
    ratio_holes = 1.0 * np.sum(mask) / img.size
    if ratio_holes > 0.1:
        warnings.warn('holes take up too much of image ({:.3f}), will be slow'.format(ratio_holes))
    warnings.filterwarnings('ignore', r'All-NaN slice encountered')
    dtype_orig = img.dtype
    img = img.astype(float)
    img[mask] = np.nan
    while np.sum(mask) > 0:
        img_median = img.copy()
        for x, y in zip(*np.nonzero(mask)):
            img_median[x, y] = np.nanmedian(img[
                                            max(0, x - 1):min(img.shape[0], x + 2),
                                            max(0, y - 1):min(img.shape[1], y + 2)
                                            ])
        img = img_median
        mask = np.isnan(img)
    return img.astype(dtype_orig)


def nearest(img):
    """
    Fill each empty pixel with its nearest neighbor. 270 ms/img.
    """
    import scipy.interpolate
    mask = img == 0
    img = img.copy()
    coors_ok = np.array(np.nonzero(mask == 0)).T
    coors_fill = np.array(np.nonzero(mask)).T
    interpolator = scipy.interpolate.NearestNDInterpolator(coors_ok, img[mask == 0])
    img[mask] = interpolator(coors_fill)
    return img


def smooth(img):
    """
    Fill holes with some smart continuous values. Also has problem with big holes 370 ms/img.
    """
    import skimage
    from skimage.restoration import inpaint_biharmonic
    mask = img == 0
    if np.sum(mask) > img.size / 20:
        warnings.warn('small holes only!')
        return None
    img = inpaint_biharmonic(img, mask)  # outputs some weird floats
    warnings.filterwarnings('ignore', 'Possible precision loss when converting from float64 to uint16')
    img = skimage.img_as_uint(img)
    return img
