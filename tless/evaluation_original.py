# just a copy-pasted original function from test.py; contains Hodan's not-good-enough evaluation


def draw_text(im, text, scale, pos, corner, font, color, thick, color_bg=None):
    thick_bg = thick + 1
    size, _ = cv2.getTextSize(text, font, scale, thick_bg)
    if corner == 'up':
        pos = (pos[0], pos[1] + size[1])
    pos = tuple(map(int, pos))
    if color_bg is not None:
        alpha_bg = 0.5
        bb = [pos[1] - size[1], pos[0], pos[1] + 3, 1 * pos[0] + size[0] - 3]
        im_bg = im[bb[0]:bb[2], bb[1]:bb[3]].copy()
        im_bg[:] = color_bg
        im[bb[0]:bb[2], bb[1]:bb[3], :] = (1 - alpha_bg) * im[bb[0]:bb[2], bb[1]:bb[3]] + alpha_bg * im_bg
    cv2.putText(im, text, pos, font, scale, (0, 0, 0), thick_bg, cv2.LINE_AA)
    cv2.putText(im, text, pos, font, scale, color, thick, cv2.LINE_AA)


def draw_rect(im, bb, color, thick):
    cv2.rectangle(im, (bb[0], bb[1]), (bb[2], bb[3]), color, thick, cv2.LINE_AA)


def draw_class(im, det, score, cls, err, gt, fn, colors=None):
    """Visual debugging of detections."""

    color_white = (255, 255, 255)
    color_red = (0, 0, 255)
    color_green = (0, 255, 0)

    # semi-transparent ground-truth bounding-boxes to mark tp / fn
    alpha_gt = 0.2
    im_gt = im.copy()
    for i in range(gt.shape[0]):
        bb = gt[i]
        color_rect = color_red if i in fn else color_green
        draw_rect(im_gt, bb, color_rect, 6)
    im[:] = (1 - alpha_gt) * im + alpha_gt * im_gt

    # draw detections
    for i in range(det.shape[0]):
        bbox = det[i]
        s = score[i]
        color_rect = color_green if colors is None else colors[i]
        draw_rect(im, bbox, color_rect, 2)
        # background marks tp / fp
        color_bg = color_green if err[i] == 0 else color_red
        draw_text(im, cls[i], 1, (bbox[0], bbox[1]), 'up', cv2.FONT_HERSHEY_DUPLEX, color_white, 1, color_bg)
        draw_text(im, '{:.3f}'.format(s), 1, (bbox[0], bbox[3]), 'down', cv2.FONT_HERSHEY_PLAIN, color_white, 1)


def get_colors(n):
    import colorsys
    colors = np.zeros((n, 3), float)
    for i in range(n):
        phi = float(i) / n * 2 * np.pi
        colors[i, :] = colorsys.yiq_to_rgb(0.5, 0.5957 * np.cos(phi), 0.5226 * np.sin(phi))
    colors *= 255
    colors = colors[::-1].astype(np.uint8)
    np.random.seed(42)
    colors[:, :] = colors[np.random.choice(n, n, replace=False), :]
    return colors.astype(int)


def ratio(a, b):
    """
    Division by zero is fine.
    :return: a/0 = np.inf, 0/0 = np.nan
    """
    rt = lambda x: x if type(x) == np.ndarray else np.array(x)
    with np.errstate(divide='ignore', invalid='ignore'):
        return 1.0 * rt(a) / rt(b)


def compute_iou(bb_gt, bb_det):
    """
    Compute intersection over union for each pair of ground-truth bounding-boxes and detected bbs.
    :param bb_gt: ground-truth bounding-boxes
    :param bb_det: detected bounding-boxes
    :return: matrix of ious, ious corresponding to one gt-bb are in a row, similarly for detected in a column
    """

    def bb_area(x1, y1, x2, y2):
        return max(0, 1.0 * (x2 - x1) * (y2 - y1))  # intersection could be negative, overflow might happen

    ious = np.zeros((len(bb_gt), len(bb_det)), dtype=float)
    for (i, j), _ in np.ndenumerate(ious):
        a_gt = bb_area(*bb_gt[i])
        a_det = bb_area(*bb_det[j, :4])
        inter = bb_area(
            max(bb_gt[i, 0], bb_det[j, 0]),
            max(bb_gt[i, 1], bb_det[j, 1]),
            min(bb_gt[i, 2], bb_det[j, 2]),
            min(bb_gt[i, 3], bb_det[j, 3]),
        )
        union = a_gt + a_det - inter
        ious[i, j] = inter / union

    return ious


def save_img(img, path):
    save_root = os.path.split(path)[0]
    if not os.path.exists(save_root):
        os.makedirs(save_root)
    cv2.imwrite(path, img)


def test_net(net, imdb, output_dir, thresh=0.2, use_depth=False, vis=False):
    """Test a Fast R-CNN network on an image database."""

    from datasets.tless import TLess
    assert isinstance(imdb, TLess), 'sorry, only t-less from now on'

    num_images = len(imdb.image_index)
    scores = array.array('f', [])  # score of each detected bb
    errs = array.array('B', [])  # type of error of each bb, see details below
    clss = array.array('B', [])  # class of each bb
    clss_gt = array.array('B', [])  # assigned gt class
    gt_cls_count = np.zeros(imdb.num_classes - 1, np.uint16)  # number of each gt class (for recall)
    conmat = np.zeros((imdb.num_classes, imdb.num_classes - 1), np.uint32)  # confusion matrix (gt x det)

    """
    errs:
        0: true-positive
        1: localisation error (0.1 <= iou < 0.5)
        2: similar class
        3: other class
        4: background
    """

    cls_similar_sets = [
        [1, 2, 3, 4],
        [5, 6],
        [11, 12],
        [13, 14, 15, 16],
        [17, 18],
        [19, 20],
        [21, 22],
        [25, 26],
        [27, 28, 29]
    ]
    cls_similar = {}
    for i_cls in range(1, 31):
        cls_similar[i_cls] = []
        for s in cls_similar_sets:
            if i_cls in s:
                cls_similar[i_cls].extend(s[:])
                cls_similar[i_cls].remove(i_cls)

    # timers
    _t = {'im_detect': Timer(), 'misc': Timer()}

    iou_almost = 0.1
    iou_thresh = 0.5
    i_max_len = int(np.ceil(np.log10(num_images)))
    colors = get_colors(imdb.num_classes - 1)

    for i_img in range(num_images):

        img = cv2.imread(imdb.image_path_at(i_img))
        img_depth = cv2.imread(
            imdb.image_path_at(i_img).replace(os.sep + 'rgb' + os.sep, os.sep + 'depth' + os.sep),
            cv2.IMREAD_UNCHANGED) if use_depth else None
        _t['im_detect'].tic()
        score, det = im_detect(net, img, None, img_depth)
        _t['im_detect'].toc()

        _t['misc'].tic()

        # kick out background (cls=0), reshape
        cls = np.tile(np.arange(1, score.shape[1], dtype=np.uint8), score.shape[0])
        score = score[:, 1:].flatten()
        det = det[:, 4:].reshape((-1, 4))

        # forget unworthy
        i_keep = score > thresh
        cls = cls[i_keep]
        score = score[i_keep]
        det = det[i_keep]

        # non max suppress
        i_keep = nms(np.hstack((det, score.reshape((-1, 1)))), cfg.TEST.NMS)
        cls = cls[i_keep]
        score = score[i_keep]
        det = det[i_keep]

        # desc sort by score
        i_sort = np.argsort(score)[::-1]
        cls = cls[i_sort]
        score = score[i_sort]
        det = det[i_sort]

        # greedy decide if each det is tp / fp
        gt_cls, gt = imdb.get_annotation(i_img)
        i_gt_aval = list(range(len(gt_cls)))
        iou = compute_iou(gt, det)
        err = []
        cls_gt = []
        for i_det in range(len(det)):
            if len(i_gt_aval) > 0:
                i_gt_sort = np.argsort(iou[:, i_det])[::-1]
                i_gt_select = [i_gt for i_gt in i_gt_sort if i_gt in i_gt_aval][0]
                if iou[i_gt_select, i_det] >= iou_almost:
                    if cls[i_det] == gt_cls[i_gt_select]:
                        if iou[i_gt_select, i_det] >= iou_thresh:
                            err.append(0)
                            i_gt_aval.remove(i_gt_select)
                        else:
                            err.append(1)
                    else:
                        if cls[i_det] in cls_similar[cls[i_det]]:
                            err.append(2)
                        else:
                            err.append(3)
                    cls_gt.append(gt_cls[i_gt_select])
                else:
                    err.append(4)
                    cls_gt.append(0)
            else:
                err.append(4)
                cls_gt.append(0)
            conmat[cls_gt[-1], cls[i_det] - 1] += 1

        clss.extend(cls)
        clss_gt.extend(cls_gt)
        scores.extend(score)
        errs.extend(err)

        for i_gt_cls in gt_cls:
            gt_cls_count[i_gt_cls - 1] += 1

        if vis:
            draw_class(img, det, score, [imdb.classes[c] for c in cls], err, gt, i_gt_aval, colors)
            save_img(img, os.path.join(output_dir, 'vis', imdb.image_index[i_img] + '.jpg'))

        _t['misc'].toc()

        r = ratio(sum([e == 0 for e in err]), len(gt))
        p = ratio(sum([e == 0 for e in err]), len(det))
        print('im_detect: {:{}d}/{:d} {:.3f}s {:.3f}s {:5.3f}r {:5.3f}p'.format(
            i_img + 1, i_max_len, num_images, _t['im_detect'].average_time, _t['misc'].average_time, r, p
        ))

    print('Evaluating detections')

    clss = np.array(clss, np.uint8)
    clss_gt = np.array(clss_gt, np.uint8)
    scores = np.array(scores)
    errs = np.array(errs, np.uint8)

    # desc sort by score
    i_sort = np.argsort(scores)[::-1]
    clss = clss[i_sort]
    clss_gt = clss_gt[i_sort]
    scores = scores[i_sort]
    errs = errs[i_sort]

    recall_cls = np.zeros((imdb.num_classes - 1, len(errs)), float)
    precision_cls = recall_cls.copy()
    ap_cls = np.zeros(imdb.num_classes - 1, float)
    for i_cls in range(1, imdb.num_classes):
        tps_cls = np.logical_and(clss == i_cls, errs == 0)
        tps_cls_cum = np.cumsum(tps_cls, dtype=np.uint32)
        recall_cls[i_cls - 1] = ratio(tps_cls_cum, gt_cls_count[i_cls - 1])
        ps_cls_cum = np.cumsum(clss == i_cls, dtype=np.uint32)
        precision_cls[i_cls - 1] = ratio(tps_cls_cum, ps_cls_cum)
        # ap of class is nan when there is no true-positive
        ap_cls[i_cls - 1] = np.mean(ratio(tps_cls_cum[tps_cls], ps_cls_cum[tps_cls]))

    mR = np.nanmean(recall_cls[:, -1])
    mAP = np.nanmean(ap_cls)

    # print a nice table
    pd_df_data = np.array([ap_cls, recall_cls[:, -1]]).T
    pd_df = pd.DataFrame(pd_df_data, imdb.classes[1:], ['ap', 'recall'])
    print('')
    with pd.option_context("display.float_format", '{:.3f}'.format):
        print(pd_df)
    print('')
    print('mR: {:.3f}\nmAP: {:.3f}'.format(mR, mAP))

    with open(os.path.join(output_dir, 'evaluation.pkl'), 'w') as f:
        cPickle.dump({
            'clss': clss, 'clss_gt': clss_gt, 'scores': scores, 'conmat': conmat,
            'errs': errs, 'precision_cls': precision_cls, 'ap_cls': ap_cls,
            'recall_cls': recall_cls, 'gt_cls_count': gt_cls_count, 'mR': mR, 'mAP': mAP,
        }, f)
