import cv2
import numpy as np
import sys
import warnings

sys.path.append('/mnt/home.stud/haluzpav/darwin_iros15_release/src/pytlod/')
from tlodlib.surface_normals_c import quantized_normals


def sobel(img, dx, dy, ksize=3):
    assert dx + dy == 1
    return cv2.Sobel(img, cv2.CV_32F, dx, dy, ksize=ksize)


def sobel3_absxy(img):
    sx = sobel(img, 1, 0)
    sy = sobel(img, 0, 1)
    return np.sqrt(sx ** 2 + sy ** 2).astype(np.uint16)


def normals(img, threshold_distance=15000, threshold_difference=4000):
    norms_quantized, norms = quantized_normals(img, threshold_distance, threshold_difference)
    return norms


def normals_xyz(img, *args):
    if not args:
        warnings.warn('normals using default thresholds')
    norms = normals(img, *args)
    norms += 1
    norms *= 256 / 2.0
    norms[norms == 256] = 255
    return norms.astype(np.uint8)


def normals_xyz2(img, *args):
    # placeholder to not overwrite original (not entirely correct) normals
    return normals_xyz(img, *args)
