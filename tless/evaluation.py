import numpy as np
import os
import cPickle
import yaml
import pandas as pd
import sys
import argparse
import cv2

import _init_paths
import misc
import score

cfg = _init_paths.import_cfg()

_init_paths.frcnn_paths()
# noinspection PyUnresolvedReferences
from nms.cpu_nms import cpu_nms

import plot_cm


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('roots', nargs='+')
    parser.add_argument('--vis_tp', action='store_true')
    parser.add_argument('--vis_fp', action='store_true')
    parser.add_argument('--vis_fn', action='store_true')
    parser.add_argument('--dataset', default='tless')
    args = parser.parse_args()
    global cfg
    cfg = _init_paths.import_cfg(args.dataset)
    return args


def load_det(path):
    print('Loading detections')
    path = os.path.join(path, 'detections.pkl')
    with open(path) as f:
        return cPickle.load(f)


def load_gt(name_list_img, list_cls=None):
    if list_cls is None:
        list_cls = list(range(1, cfg.n_cls + 1))
    print('Loading ground-truths')

    path_cache = os.path.join(cfg.root_root, 'cache', name_list_img + '.pkl')
    if os.path.exists(path_cache):
        with open(path_cache) as f:
            print('Loaded gt from cache')
            return cPickle.load(f)

    gt = []
    path = os.path.join(cfg.root, 'lists', name_list_img + '.txt')
    with open(path) as file_list:
        # assuming paths in the list are sorted, otherwise highly inefficient
        path_group = None
        for path_img in file_list:
            # load new yaml if different needed
            path_group_new = os.path.abspath(os.path.join(path_img, '..', '..'))  # e.g. 'obj_01'
            if path_group != path_group_new:
                print('.')
                path_group = path_group_new
                with open(os.path.join(path_group, 'gt.yml')) as file_gt:
                    data = yaml.load(file_gt, Loader=yaml.CLoader)

            i_img = int(os.path.splitext(os.path.split(path_img)[1])[0])
            n_obj = sum(1 for o in data[i_img] if o['obj_id'] in list_cls)
            bbox = np.zeros((n_obj, 4), dtype=np.uint16)
            cls = np.zeros(n_obj, dtype=np.uint8)
            for i_obj, obj in enumerate(data[i_img]):
                if obj['obj_id'] not in list_cls:
                    continue
                bb = obj['obj_bb']
                bbox[i_obj, :] = [
                    bb[0],
                    bb[1],
                    bb[0] + bb[2],
                    bb[1] + bb[3]
                ]
                cls[i_obj] = obj['obj_id']
            gt.append({
                'cls': cls,
                'bbox': bbox
            })

    print('Saving gt cache')
    with open(path_cache, 'w') as f:
        cPickle.dump(gt, f)

    return gt


def load_next_image(name_list, i):
    try:
        load_next_image.list
    except AttributeError:
        # BEHOLD! GLORIOUS PYTHON SYNTAX!! (static variable)
        with open(os.path.join(cfg.root, 'lists', name_list + '.txt')) as f:
            load_next_image.list = f.readlines()
    # print(next(load_next_image.list))
    return cv2.imread(load_next_image.list[i].strip(), cv2.IMREAD_UNCHANGED)


def get_similar_classes():
    cls_similar_sets = [
        [1, 2, 3, 4],
        [5, 6],
        [11, 12],
        [13, 14, 15, 16],
        [17, 18],
        [19, 20],
        [21, 22],
        [25, 26],
        [27, 28, 29]
    ]
    cls_similar = {}
    for i_cls in range(1, 31):
        cls_similar[i_cls] = []
        for s in cls_similar_sets:
            if i_cls in s:
                cls_similar[i_cls].extend(s[:])
                cls_similar[i_cls].remove(i_cls)
    return cls_similar


def filter_det(det, thresh_nms=0.7):
    dc, ds, db = det

    # non max suppress
    i_keep = cpu_nms(np.hstack((db, ds.reshape((-1, 1)))), thresh_nms)
    dc = dc[i_keep]
    ds = ds[i_keep]
    db = db[i_keep]

    return dc, ds, db


def compute_iou(bb_gt, bb_det):
    """
    Compute intersection over union for each pair of ground-truth bounding-boxes and detected bbs.
    :param bb_gt: ground-truth bounding-boxes
    :param bb_det: detected bounding-boxes
    :return: matrix of ious, ious corresponding to one gt-bb are in a row, similarly for detected in a column
    """

    def bb_area(x1, y1, x2, y2):
        if x1 >= x2 or y1 >= y2:
            return 0
        else:
            return 1.0 * (x2 - x1) * (y2 - y1)

    ious = np.zeros((len(bb_gt), len(bb_det)), dtype=float)
    for (i, j), _ in np.ndenumerate(ious):
        a_gt = bb_area(*bb_gt[i])
        a_det = bb_area(*bb_det[j, :4])
        inter = bb_area(
            max(bb_gt[i, 0], bb_det[j, 0]),
            max(bb_gt[i, 1], bb_det[j, 1]),
            min(bb_gt[i, 2], bb_det[j, 2]),
            min(bb_gt[i, 3], bb_det[j, 3]),
        )
        union = a_gt + a_det - inter
        ious[i, j] = inter / union

    return ious


def evaluate_image(det, gt, thresh_iou_almost=0.3, thresh_iou=0.7):
    dc, ds, db = det  # class, score, bounding-box
    gc, gb = gt

    try:
        evaluate_image.cls_similar
    except AttributeError:
        evaluate_image.cls_similar = get_similar_classes()

    n_det = len(dc)

    """
    errs:
        0: true-positive
        1: localisation error (thresh_iou_almost <= iou < thresh_iou), same class
        2: similar class, iou >= thresh_iou_almost
        3: other class, iou >= thresh_iou_almost
        4: background
    """

    # greedy decide if each det is tp / fp
    i_gt_aval = list(range(len(gc)))
    iou = compute_iou(gb, db)
    err = np.zeros(n_det, np.uint8)
    cls_assigned = np.zeros(n_det, np.uint8)
    i_gt_assigned = np.zeros(n_det, np.int8) - 1  # no assigned gt by default
    for i_det in range(n_det):
        cls_det = dc[i_det]
        if len(i_gt_aval) > 0:
            i_gt_sort = np.argsort(iou[:, i_det])[::-1]
            i_gt_select = [i_gt for i_gt in i_gt_sort if i_gt in i_gt_aval][0]
            cls_gt = gc[i_gt_select]
            if iou[i_gt_select, i_det] >= thresh_iou_almost:
                cls_assigned[i_det] = cls_gt
                if cls_det == cls_gt:
                    if iou[i_gt_select, i_det] >= thresh_iou:
                        err[i_det] = 0
                        i_gt_aval.remove(i_gt_select)
                        i_gt_assigned[i_det] = i_gt_select
                    else:
                        err[i_det] = 1
                else:
                    if cls_det in evaluate_image.cls_similar[cls_det]:
                        err[i_det] = 2
                    else:
                        err[i_det] = 3
                continue
        err[i_det] = 4
        cls_assigned[i_det] = 0

    return cls_assigned, err, i_gt_assigned


def ratio(a, b):
    """
    Division by zero is fine.
    :return: a/0 = np.inf, 0/0 = np.nan
    """

    def assure_type(x):
        return np.array(x, float, copy=False)

    with np.errstate(divide='ignore', invalid='ignore'):
        return 1.0 * assure_type(a) / assure_type(b)


def decor_ap(ap_method):
    def wrapper(*args, **kwargs):
        i_tp = kwargs.get('i_tp') or args[-1]
        if np.sum(i_tp) == 0:
            return np.nan
        return ap_method(*args, **kwargs)

    return wrapper


@decor_ap
def ap_hodan(p, i_tp):
    """
    Average precision for a single class. Assume recall reaches 1. Basically integrate under recall-precision curve.
    :param p: precision, assume desc sorted by score
    :param i_tp: boolean array marking true-positives
    """
    return np.mean(p[i_tp])


@decor_ap
def ap_haluza(r, p, i_tp):
    """
    Same as *ap_hodan*, plus scale to the actual maximum recall.
    """
    return ap_hodan(p, i_tp) * r[-1]


@decor_ap
def ap_pascal_haluza(r, p, i_tp):
    """
    http://host.robots.ox.ac.uk/pascal/VOC/voc2012/htmldoc/devkit_doc.html#SECTION00044000000000000000
    Basically convert precision as if you shined light at the curve from the right - new curve is shadow outline.
    That will make it monotonically decreasing, which is good for some reason.
    Then integrate under this new curve.
    """
    # p = p[i_tp]
    # p_revers = p[::-1]
    # p_revers_monotonic_increase = np.maximum.accumulate(p_revers)
    # p_monotonic_decrease = p_revers_monotonic_increase[::-1]
    # return np.mean(p_monotonic_decrease) * r[-1]

    # noinspection PyArgumentList
    return np.mean(np.maximum.accumulate(p[i_tp][::-1])[::-1]) * r[-1]


def ap_pascal_hodan(r, p):
    """
    Pascal 2010+ method implemented by Hodan.
    """
    return score.ap(r, p)


def rpap(dc, err, n_gt, cls=0):
    if cls:
        i_p = dc == cls
        i_tp = np.logical_and(dc == cls, err == 0)
    else:
        i_p = np.ones(dc.shape, np.bool)
        i_tp = err == 0

    n_p = np.cumsum(i_p, dtype=np.uint32)
    n_tp = np.cumsum(i_tp, dtype=np.uint32)

    recall = ratio(n_tp, n_gt) if n_gt > 0 else n_tp * np.nan  # don't want any ugly infinities
    precision = ratio(n_tp, n_p)  # can't contain any infs
    ap = np.array((
        ap_hodan(precision, i_tp),
        ap_haluza(recall, precision, i_tp),
        ap_pascal_haluza(recall, precision, i_tp),
        ap_pascal_hodan(recall, precision)
    ))

    return recall, precision, ap


def draw_text(im, text, scale, pos, corner, font, color, thick, color_bg=None):
    thick_bg = thick + 1
    size, _ = cv2.getTextSize(text, font, scale, thick_bg)
    if corner == 'up':
        pos = (pos[0], pos[1] + size[1])
    pos = tuple(map(int, pos))
    if color_bg is not None:
        alpha_bg = 0.5
        bb = [pos[1] - size[1], pos[0], pos[1] + 3, 1 * pos[0] + size[0] - 3]
        im_bg = im[bb[0]:bb[2], bb[1]:bb[3]].copy()
        im_bg[:] = color_bg
        im[bb[0]:bb[2], bb[1]:bb[3], :] = (1 - alpha_bg) * im[bb[0]:bb[2], bb[1]:bb[3]] + alpha_bg * im_bg
    cv2.putText(im, text, pos, font, scale, (0, 0, 0), thick_bg, cv2.LINE_AA)
    cv2.putText(im, text, pos, font, scale, color, thick, cv2.LINE_AA)


def draw_rect(im, bb, color, thick):
    cv2.rectangle(im, (bb[0], bb[1]), (bb[2], bb[3]), color, thick, cv2.LINE_AA)


def draw_img(im, det, score, cls, err, i_gt, gt, name_cls, thresh_score_fp=0.5, vis_tp=True, vis_fp=True, vis_fn=True):
    """Visual debugging of detections."""

    try:
        draw_img.colors
    except AttributeError:
        draw_img.colors = get_colors(len(name_cls))

    color_white = (255, 255, 255)
    color_red = (0, 0, 255)
    color_green = (0, 255, 0)

    if vis_fn:
        # semi-transparent ground-truth bounding-boxes to mark fn
        alpha_gt = 0.2
        im_gt = im.copy()
        for i in range(gt.shape[0]):
            if i in i_gt:
                # don't show tp gt
                continue
            bb = gt[i]
            color_rect = color_red if i not in i_gt else color_green
            draw_rect(im_gt, bb, color_rect, 6)
        im[:] = (1 - alpha_gt) * im + alpha_gt * im_gt

    if vis_tp or vis_fp:
        # draw detections
        for i in range(det.shape[0]):
            bbox = det[i]
            s = score[i]
            is_tp = err[i] == 0
            if not is_tp and s < thresh_score_fp:
                # don't show all fp since that would be a complete mess
                continue
            if (is_tp and not vis_tp) or (not is_tp and not vis_fp):
                continue
            color_rect = draw_img.colors[cls[i] - 1]
            draw_rect(im, bbox, color_rect, 2)
            # background marks tp / fp
            color_bg = color_green if is_tp else color_red
            draw_text(im, name_cls[cls[i] - 1], 1, (bbox[0], bbox[1]), 'up', cv2.FONT_HERSHEY_DUPLEX, color_white, 1,
                      color_bg)
            draw_text(im, '{:.3f}'.format(s), 1, (bbox[0], bbox[3]), 'down', cv2.FONT_HERSHEY_PLAIN, color_white, 1)


def get_colors(n):
    import colorsys
    colors = np.zeros((n, 3), float)
    for i in range(n):
        phi = float(i) / n * 2 * np.pi
        colors[i, :] = colorsys.yiq_to_rgb(0.5, 0.5957 * np.cos(phi), 0.5226 * np.sin(phi))
    colors *= 255
    colors = colors[::-1].astype(np.uint8)
    np.random.seed(42)
    colors[:, :] = colors[np.random.choice(n, n, replace=False), :]
    return colors.astype(int)


def save_img(img, path):
    save_root = os.path.split(path)[0]
    if not os.path.exists(save_root):
        os.makedirs(save_root)
    cv2.imwrite(path, img)


def evaluate(root_det, name_list=None, vis_tp=False, vis_fp=False, vis_fn=False):
    print('Starting evaluating ' + root_det)
    if name_list is None:
        name_dir = os.path.split(root_det)[-1]
        i_end = name_dir.find('_iter')
        i_end = len(name_dir) if i_end < 0 else i_end
        name_list = name_dir[:i_end]

    det = load_det(root_det)
    n_img = len(det)
    n_det = sum(len(di['cls']) for di in det.values())

    gt = load_gt(name_list, list(range(1, cfg.n_cls + 1)))
    assert len(gt) == n_img, 'Mismatch number of gt ({}) and det ({}) images'.format(len(gt), n_img)

    print('Evaluating detections')

    cls_det = []
    score = []
    bbox = []
    cls_gt = []
    err = []
    conmat = np.zeros((cfg.n_cls + 1, cfg.n_cls), np.uint32)  # confusion matrix (gt x det)

    for i_img in range(n_img):
        print('Evaluating {:d} / {:d}'.format(i_img, n_img))
        dc, ds, db = [det[i_img][t] for t in ('cls', 'score', 'bbox')]
        gc, gb = [gt[i_img][t] for t in ('cls', 'bbox')]

        dc, ds, db = filter_det((dc, ds, db))

        cls_assigned, e, i_gt = evaluate_image((dc, ds, db), (gc, gb))

        cls_det.extend(dc)
        score.extend(ds)
        bbox.extend(db)
        cls_gt.extend(cls_assigned)
        err.extend(e)

        is_all_gt_used = all(i in i_gt for i in range(len(gc)))
        if vis_tp or vis_fp or vis_fn:
            img = load_next_image(name_list, i_img)
            draw_img(img, db, ds, dc, e, i_gt, gb, ['{:02d}'.format(c) for c in range(1, cfg.n_cls + 1)],
                     vis_tp=vis_tp, vis_fp=vis_fp, vis_fn=vis_fn)
            save_img(img, os.path.join(root_det, 'vis', '{:04d}.jpg'.format(i_img)))

    print('Summarizing evaluations')

    cls_det = np.array(cls_det, np.uint8)
    score = np.array(score)
    bbox = np.array(bbox)
    cls_gt = np.array(cls_gt, np.uint8)
    err = np.array(err, np.uint8)

    # desc sort by score
    i_sort = np.argsort(score)[::-1]
    cls_det = cls_det[i_sort]
    score = score[i_sort]
    bbox = bbox[i_sort]
    cls_gt = cls_gt[i_sort]
    err = err[i_sort]

    # first compute recall, precision per class, then make means
    n_det = len(score)
    recall = np.zeros((cfg.n_cls, n_det), float)
    precision = recall.copy()
    ap_cls = np.zeros((4, cfg.n_cls), float)
    n_gt_cls = np.zeros(cfg.n_cls, np.uint32)
    for i_cls in range(1, cfg.n_cls + 1):
        # class labels are 1-based (index=0 is background)
        n_gt = np.sum(np.sum(np.equal(gt[i_img]['cls'], i_cls)) for i_img in range(n_img))
        recall[i_cls - 1], precision[i_cls - 1], ap_cls[:, i_cls - 1] = rpap(cls_det, err, n_gt, i_cls)
        for i_cls_gt in range(0, cfg.n_cls + 1):
            conmat[i_cls_gt, i_cls - 1] += np.sum(np.logical_and(cls_det == i_cls, cls_gt == i_cls_gt))
        n_gt_cls[i_cls - 1] = n_gt
    mR_perclass = np.nanmean(recall[:, -1])
    mAP_perclass = np.nanmean(ap_cls, axis=1)

    # compute all at once
    n_gt = np.sum(len(gt[i_img]['cls']) for i_img in range(n_img))
    recall_overall, precision_overall, mAP_overall = rpap(cls_det, err, n_gt)
    mR_overall = recall_overall[-1]

    print('Printing summary')

    # print a nice tables
    dataframe_pr = pd.DataFrame(
        np.vstack((
            np.hstack((
                recall[:, -1],
                mR_perclass,
                mR_overall,
            )),
            np.hstack((
                ap_cls,
                mAP_perclass[:, np.newaxis],
                mAP_overall[:, np.newaxis],
            )),
        )),
        ['recall', 'ap_hodan', 'ap_haluza', 'ap_pashal', 'ap_pashod'],
        ['{:02d}'.format(c) for c in range(1, cfg.n_cls + 1)] + ['perclass', 'overall']
    )
    dataframe_conmat = pd.DataFrame(
        conmat,
        ['{:02d}'.format(c) for c in range(0, cfg.n_cls + 1)],
        ['{:02d}'.format(c) for c in range(1, cfg.n_cls + 1)]
    )
    dataframe_numbers = pd.DataFrame(
        np.vstack((
            np.hstack((0, np.sum(conmat, axis=0))),
            np.sum(conmat, axis=1),
            np.hstack((0, n_gt_cls)),
        )),
        ['n_det', 'n_det_gt', 'n_gt'],
        ['{:02d}'.format(c) for c in range(0, cfg.n_cls + 1)],
        np.uint32
    )
    dataframe_numbers['sum'] = dataframe_numbers.sum(axis=1)
    print('')
    with pd.option_context('display.float_format', '{:.3f}'.format, 'display.max_columns', 100, 'display.width', 1000):
        with open(os.path.join(root_det, 'evaluation_summary.txt'), 'w') as f:
            def p(s): misc.prints(s, file=[sys.stdout, f])

            p('Recall / precision')
            p(dataframe_pr)
            p('\nConfusion matrix (gt, det)')
            p(dataframe_conmat)
            p('\nVarious numbers')
            p(dataframe_numbers)

    print('Plotting confusion matrix')

    plot_cm.plot_cm(conmat, os.path.join(root_det, 'cm.pdf'))

    print('Saving evaluations')

    scope = locals()  # haaack, don't dare to touch this
    data_dict = dict((k, eval(k, scope)) for k in (
        'cls_det', 'score', 'bbox', 'cls_gt', 'err', 'recall', 'precision', 'ap_cls', 'mR_perclass', 'mAP_perclass',
        'recall_overall', 'precision_overall', 'mAP_overall', 'mR_overall', 'conmat'
    ))
    with open(os.path.join(root_det, 'evaluation.pkl'), 'w') as f:
        cPickle.dump(data_dict, f)

    print('')


def main():
    args = parse_args()
    print('Using' + str(args))
    for root in args.roots:
        evaluate(root, vis_tp=args.vis_tp, vis_fp=args.vis_fp, vis_fn=args.vis_fn)


if __name__ == '__main__':
    main()
