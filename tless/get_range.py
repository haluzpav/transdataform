import cv2
import os
import numpy as np
import glob
import argparse
import cPickle

import _init_paths

cfg = _init_paths.import_cfg()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('name_dir')
    parser.add_argument('img_type')
    parser.add_argument('--ignore_zero', action='store_true')
    parser.add_argument('--zero_value', type=int, default=0)
    parser.add_argument('--range_compare', nargs=2, type=int)
    parser.add_argument('--dataset', default='tless')

    args = parser.parse_args()

    global cfg
    cfg = _init_paths.import_cfg(args.dataset)

    return args


def range_img(img, ignore_zero=False, zero_value=0):
    if ignore_zero:
        img = img.astype(float)
        img[img == zero_value] = np.nan
    return np.nanmin(np.nanmin(img, 0), 0), np.nanmax(np.nanmax(img, 0), 0)


def get_range(name_dir, img_type, ignore_zero=False, zero_value=0, range_compare=None):
    path_mask = os.path.join(cfg.root, name_dir, '*', img_type, '*')
    mins, maxs, outs = None, None, None
    paths_img = sorted(glob.glob(path_mask))
    for i_img, path_img in enumerate(paths_img):
        img = cv2.imread(path_img, cv2.IMREAD_UNCHANGED)
        if mins is None or maxs is None or outs is None:
            mins = np.zeros((len(paths_img), 1 if len(img.shape) == 2 else img.shape[2]), img.dtype)
            maxs = mins.copy()
            outs = mins.astype(np.uint64, copy=True)
        mins[i_img], maxs[i_img] = range_img(img, ignore_zero, zero_value)
        if range_compare:
            outs[i_img] = np.sum(np.logical_and(
                np.logical_or(img > 0, not ignore_zero),
                np.logical_or(img < range_compare[0], img > range_compare[1])
            ))
        if i_img % 100 == 0:
            print(path_img)
            # if i_img == 1000:
            #     break
            # if np.any(maxs[i_img] == 0):
            #     pass
            # if np.any(mins[i_img, :2] < 50):
            #     pass
            # if np.any(mins[i_img, :2] > 200):
            #     pass
    for m in (mins, maxs):
        print(m.min(0), m.max(0), m.mean(0), m.std(0), np.median(m, 0), np.sum(outs))

    with open(os.path.join(cfg.root, name_dir, 'stats_' + img_type + '.pkl'), 'w') as f:
        cPickle.dump({'min': mins, 'max': maxs, 'out': outs}, f)


def main():
    args = parse_args()
    get_range(args.name_dir, args.img_type, args.ignore_zero, args.zero_value, args.range_compare)


if __name__ == '__main__':
    main()
