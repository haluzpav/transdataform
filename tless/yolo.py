import glob
import os

import yaml

import cfg
import misc


def gen_labels(obj_ids, train, nums_test):
    """
    generates a file for each image describing objects
    :param obj_ids: to be labeled, in scenes others are ignored
    :param train: bool, should label train data
    :param nums_test: scene ids
    :return:
    """
    if obj_ids is None:
        obj_ids = list(range(1, 31, 1))
    if train is None:
        train = False
    if nums_test is None:
        nums_test = list(range(1, 21, 1))
    for path in glob.glob(os.path.join(cfg.root, '*', '*')):
        label = os.path.split(path)[1]
        if label.startswith('obj'):
            is_obj = True
        elif label.startswith('scene'):
            is_obj = False
        else:
            continue
        if os.path.isfile(path):
            # TODO better fix - check the path is relevant
            continue
        lid = int(label.split('_')[-1])
        if not train and is_obj:
            continue
        if is_obj and lid not in obj_ids:
            continue
        elif not is_obj and lid not in nums_test:
            continue
        gen_labels_single(path, obj_ids, is_obj)


def gen_labels_single(path, obj_ids, is_obj):
    for f in cfg.filenames_yaml_info:
        if f in os.listdir(path):
            data_path = os.path.join(path, f)
            break
    else:
        raise StandardError('Yaml file not found')
    print('relabeling ' + path)
    with open(data_path) as f:
        data = yaml.load(f, Loader=yaml.CLoader)
    root_labels = os.path.join(path, 'labels')
    misc.mkdir(root_labels)
    img_size = cfg.img_sizes[misc.split_path(path)[-2]]
    for id_img in data:
        if is_obj:
            data_img = [data[id_img]]
            data_img[0]['obj_id'] = int(path[-2:])
        else:
            data_img = data[id_img]
        filename_labels = '{:04d}.txt'.format(id_img)
        with open(os.path.join(root_labels, filename_labels), 'w') as fl:
            for obj in data_img:
                if obj['obj_id'] not in obj_ids:
                    continue
                oid = obj_ids.index(obj['obj_id'])
                bb = obj['obj_bb']
                label = ' '.join(['{:d}'] + ['{:f}'] * 4).format(
                    oid,
                    1.0 * (bb[0] + bb[2] / 2) / img_size[0],
                    1.0 * (bb[1] + bb[3] / 2) / img_size[1],
                    1.0 * bb[2] / img_size[0],
                    1.0 * bb[3] / img_size[1],
                )
                fl.write(label + '\n')


def _is_img(path):
    ends = ['png', 'jpg', 'jpeg']
    for e in ends:
        if path[-len(e):].lower() == e:
            return True
    return False


def main():
    gen_labels(None, True, None)


if __name__ == '__main__':
    main()
