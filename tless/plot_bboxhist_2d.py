import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os
import argparse
import numpy as np

import _init_paths
from tless.evaluation import load_det, load_gt
from common.plot_rpn import load_rpn
import misc
import cfg


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('root')
    parser.add_argument('--dataset', default='tless')
    args = parser.parse_args()
    global cfg
    cfg = _init_paths.import_cfg(args.dataset)
    return args


def get_bbox_i_from_gt(root):
    name_list = get_name_list(root)
    gt = load_gt(name_list)
    return np.array([bb for img in gt for bb in img['bbox']])


def get_bbox_i_from_rpn(root):
    rpn = load_rpn(root)
    return np.array([bb for i_img in rpn for bb in rpn[i_img]['bbox']])


def get_bbox_i_from_det(root):
    det = load_det(root)
    return np.array([bb for i_img in det for bb in det[i_img]['bbox']])


def get_bbox_i_from_eval(root):
    ev = misc.load_evals(root)
    return ev['bbox'][ev['err'] == 0]


def get_name_list(root):
    raw = os.path.split(root)[-1]
    i_end = raw.find('_iter')
    if i_end < 0:
        return raw
    else:
        return raw[:i_end]


def get_img_size(root):
    dir_last = os.path.split(root)[-1]
    for name_size, size in cfg.img_sizes.items():
        if name_size.startswith(dir_last[:4]):
            return size


def get_hist(bbox, size_img):
    hist = np.zeros(size_img[::-1], np.uint32)
    print(len(bbox))
    for i, bb in enumerate(bbox):
        # if i > 100:
        #     break
        # if i % 1000 == 0:
        #     print(i)
        x1, y1, x2, y2 = bb
        if not (0 <= x1 < x2 < size_img[0] or 0 <= y1 < y2 < size_img[1]):
            print(x1, y1, x2, y2)
            raw_input()
        x1, y1 = np.floor((x1, y1)).astype(int)
        x2, y2 = np.ceil((x2, y2)).astype(int)
        hist[y1:y2, x1:x2] += 1
        # hist[y1:y2, x1] += 1
        # hist[y1:y2, x2] += 1
        # hist[y1, x1-1:x2-1] += 1
        # hist[y2, x1-1:x2-1] += 1
    return hist


def plot_hist_bbox(hist, root, name_type):
    fig = plt.figure(figsize=(4, 4), dpi=300)
    ax = fig.add_subplot(1, 1, 1)
    im = ax.imshow(hist, interpolation='nearest')  # , origin='lower', extent=[0, 1, 0, 1], norm=LogNorm()
    # ax.plot(x, y, linestyle=':', marker='')
    fig.colorbar(im, cax=make_axes_locatable(ax).append_axes("right", size="5%", pad=0.1))
    # ax.set_xlabel('IoU')
    # ax.set_ylabel('objectness')
    # ax.set_xlim((0, 1))
    # ax.set_ylim((0, 1))
    # ax.grid(True, alpha=0.2)
    # ax.set_xticklabels(np.linspace(0, size_img[1], 6))
    # ax.set_yticklabels(np.linspace(0, size_img[0], 6))
    ax.xaxis.set_ticks(np.linspace(0, hist.shape[1], 6, dtype=int))
    ax.yaxis.set_ticks(np.linspace(0, hist.shape[0], 6, dtype=int))
    fig.savefig(os.path.join(root, 'hist_bbox_' + name_type + '.pdf'), bbox_inches='tight')


def plot_hist_curves(hists, root, names_type):
    for i in range(2):
        fig = plt.figure(figsize=(4, 3), dpi=300)
        # ax = fig.add_subplot(1, 2, i + 1)
        ax = fig.add_subplot(1, 1, 1)
        lines = []
        for hist, name in zip(hists, names_type):
            curve = np.sum(hist, i)
            line = ax.plot(curve, label=name)
            lines.extend(line)
        # ax.plot(x, y, linestyle=':', marker='')
        # ax.set_xlabel(('horizontal' if i == 0 else 'vertical') + ' direction')
        ax.set_xlim((0, hist.shape[1 - i]))
        ax.set_ylim((1e2, 3e8))
        ax.grid(True, alpha=0.2)
        ax.xaxis.set_ticks(np.linspace(0, hist.shape[1 - i], 6, dtype=int))
        ax.set_yscale('log')
        if i == 1:
            plt.setp(ax.get_yticklabels(), visible=False)
            ax.legend(loc='lower center')  # , bbox_to_anchor=(1.05, 0.5))
        fig.savefig(os.path.join(root, 'hist_curves{}.pdf'.format(i)), bbox_inches='tight')


def main():
    args = parse_args()
    types = ('gt', 'rpn', 'det', 'eval')
    # types = ('gt', 'eval')
    hists = []
    for t in types:
        # if t != 'eval':
        #     continue
        load_func = globals()['get_bbox_i_from_' + t]
        bbox = load_func(args.root)
        size_img = get_img_size(args.root)
        print('Computing ' + t)
        hist = get_hist(bbox, size_img)
        hists.append(hist)
        print('Plotting ' + t)
        plot_hist_bbox(hist, args.root, t)
    labels = ('ground-truth', 'proposals', 'detections', 'true positives')
    # labels = types
    plot_hist_curves(hists, args.root, labels)


if __name__ == '__main__':
    main()
