import Queue as queue  # wtf python2
import os
import threading
import argparse

import cv2
import numpy as np

import _init_paths
import misc
import inpaint
import derivate

cfg = _init_paths.import_cfg()


def parse_args(argv=None):
    inpaints = ['median', 'nearest', 'smooth']
    derivates = ['sobel3_absxy', 'normals_xyz', 'normals_xyz2']

    parser = argparse.ArgumentParser()
    parser.add_argument('name_dir')
    parser.add_argument('--ids', type=int, choices=list(range(1, 31)), nargs='+')
    parser.add_argument('--cut_elevs', type=int, nargs='+', default=[15, 5, -5, -15])
    parser.add_argument('--grayscale', action='store_true')
    parser.add_argument('--mask', action='store_true')
    parser.add_argument('--inpaint', choices=inpaints, nargs='+', default=[])
    parser.add_argument('--der_src', choices=inpaints, default='median')
    parser.add_argument('--derivate', choices=derivates, nargs='+', default=[])
    parser.add_argument('--thresholds_normals', type=int, nargs='+', default=[])
    parser.add_argument('--singlethread', action='store_true')
    parser.add_argument('--dataset', default='tless')
    args = parser.parse_args(argv)

    global cfg
    cfg = _init_paths.import_cfg(args.dataset)

    if args.ids is None:
        if args.dataset == 'tless':
            args.ids = list(range(1, cfg.n_cls + 1) if args.name_dir.startswith('train') else range(1, 21))
        elif args.dataset == 'hinter':
            args.ids = list(range(1, cfg.n_cls + 1) if args.name_dir.startswith('train') else range(1, 16))
        else:
            raise NotImplementedError

    return args


args = parse_args() if __name__ == '__main__' else parse_args([''])
root = ''


def grayscale(img):
    return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


def mask(img):
    return (img > 0).astype(np.uint8) * 255


def job_extend(i):
    path_mask = os.path.join(root, '{}', '{:04d}.png'.format(i))

    if args.grayscale:
        img = cv2.imread(path_mask.format('rgb'), cv2.IMREAD_UNCHANGED)
        gray = grayscale(img)
        cv2.imwrite(path_mask.format('grayscale'), gray)


    img = cv2.imread(path_mask.format('depth'), cv2.IMREAD_UNCHANGED)

    if args.mask:
        img_mask = mask(img)
        cv2.imwrite(path_mask.format('depth_mask'), img_mask)

    img_der_src = None

    for inp in args.inpaint:
        method = getattr(inpaint, inp)
        img_inpaint = method(img)
        if img_inpaint is None:
            raise RuntimeError('missing inpaint ' + path_mask + ' ' + inp)
        cv2.imwrite(path_mask.format('depth_inpaint_' + inp), img_inpaint)
        if inp == args.der_src:
            img_der_src = img_inpaint

    if img_der_src is None and args.derivate and args.der_src not in args.inpaint:
        path_inpaint = path_mask.format('depth_inpaint_' + args.der_src)
        if os.path.isfile(path_inpaint):
            img_der_src = cv2.imread(path_inpaint, cv2.IMREAD_UNCHANGED)
        else:
            img_der_src = getattr(inpaint, args.der_src)(img)
    if img_der_src is None and args.derivate:
        raise RuntimeError('missing inpaint for derivate ' + path_mask + ' ' + args.der_src)
    for der in args.derivate:
        method = getattr(derivate, der)
        if der.startswith('normals'):
            img_derivate = method(img_der_src, *args.thresholds_normals)
        else:
            img_derivate = method(img_der_src)
        cv2.imwrite(path_mask.format('depth_' + der), img_derivate)


def worker(que):
    while True:
        try:
            args_job = que.get(timeout=5)
        except queue.Empty:
            # so the script actually exits (after 5 secs) at the end
            return
        job_extend(*args_job)
        que.task_done()


def extend_depth():
    global root
    print('using args ' + str(args))

    if not args.singlethread:
        que = queue.Queue(20)
        for i in range(4):
            threading.Thread(target=worker, args=(que,)).start()

    root_mask = os.path.join(cfg.root, args.name_dir, '{:02d}')
    for i in args.ids:
        root = root_mask.format(i)
        assert os.path.exists(root)

        if args.grayscale:
            misc.mkdir(os.path.join(root, 'grayscale'))
        if args.mask:
            misc.mkdir(os.path.join(root, 'depth_mask'))
        for inp in args.inpaint:
            misc.mkdir(os.path.join(root, 'depth_inpaint_' + inp))
        for der in args.derivate:
            misc.mkdir(os.path.join(root, 'depth_' + der))
        len_paths = len(os.listdir(os.path.join(root, 'depth')))

        pp = misc.ProgressPrinter(root, 10, len_paths)
        for i_img in range(len_paths):
            if cfg.has_cutable_elevs and misc.elevation(i_img, args.name_dir.startswith('test')) in args.cut_elevs:
                continue
            if args.singlethread:
                job_extend(i_img)
            else:
                que.put((i_img,))
            pp.prog(i_img)
        if not args.singlethread:
            que.join()


if __name__ == '__main__':
    extend_depth()
