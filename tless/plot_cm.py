import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import itertools
import os

import _init_paths
import misc

misc.latexize_matplotlib()


def plot_cm(cm, dst_save, normalize=True, show_bg=True, show_numbers=False):
    assert cm.shape[0] == cm.shape[1] + 1, 'probably missing bg class'
    if not show_bg:
        cm = cm[:-1]

    cm_text = cm.copy()
    if normalize:
        cm = cm.astype(float) / cm.sum(axis=1)[:, np.newaxis]

    fig = plt.figure(figsize=(4, 4), dpi=300)
    ax = fig.add_subplot(1, 1, 1)
    im = ax.imshow(cm, interpolation='nearest', norm=LogNorm())
    fig.colorbar(im, cax=make_axes_locatable(ax).append_axes('right', size='5%', pad=0.1))
    ax.set_xticks(range(0, cm.shape[1] + 1))
    ax.set_yticks(range(0, cm.shape[1] + (2 if show_bg else 1)))
    classes = [('{:02d}'.format(x) if x % 2 == 0 else '') for x in np.arange(0, cm.shape[1] + 2)]
    # classes = ['{:02d}'.format(x) for x in np.arange(0, cm.shape[1] + 2)]
    classes[0] = 'bg'
    ax.set_xticklabels(classes[1:])
    ax.set_yticklabels(classes if show_bg else classes[1:])
    ax.set_xlim(xmax=cm.shape[1] - 0.5)
    ax.set_ylim(ymin=cm.shape[1] + (0.5 if show_bg else -0.5))
    ax.grid(True, alpha=0.2)
    ax.tick_params(axis='both', which='major', labelsize=8)

    if show_numbers:
        thresh = cm.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(j, i, cm_text[i, j],
                     horizontalalignment='center',
                     color='white' if cm[i, j] > thresh else 'black')

    ax.set_xlabel('Detected class')
    ax.set_ylabel('Ground truth class')

    fig.savefig(dst_save)
