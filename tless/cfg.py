import os

root_root = '/datagrid/personal/haluzpav/tless'
root = '/datagrid/personal/haluzpav/tless/t-less_v2'
root_lists = os.path.join(root, 'lists')
img_sizes = {
    'train_primesense': (400, 400),
    'test_primesense': (720, 540),
}
scale_train_to_test = 0.8
n_cls = 30
has_cutable_elevs = True
