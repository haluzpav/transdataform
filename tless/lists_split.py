import argparse
import os
import numpy as np

import _init_paths
import misc

cfg = _init_paths.import_cfg()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('name')
    parser.add_argument('dir_name')
    parser.add_argument('--id_list', type=int, nargs='+')
    parser.add_argument('--train_ratio', type=int, default=50)
    parser.add_argument('--cut_elevs', type=int, nargs='+', default=[15, 5, -5, -15])
    parser.add_argument('--dataset', default='tless')

    args = parser.parse_args()

    global cfg
    cfg = _init_paths.import_cfg(args.dataset)

    if args.id_list is None:
        if args.dataset == 'tless':
            args.id_list = list(range(1, cfg.n_cls + 1) if args.dir_name.startswith('train') else range(1, 21))
        if args.dataset == 'hinter':
            args.id_list = list(range(1, cfg.n_cls + 1) if args.dir_name.startswith('train') else range(1, 16))
        else:
            raise NotImplementedError
    if not 0 < args.train_ratio < 100:
        parser.error('incorrect train ratio')

    return args


def generate_list(name_list, name_root, i_types, train_ratio=50, cut_elevs=None):
    path_list = os.path.join(cfg.root_lists, name_list + '_' + '{:02d}'.format(train_ratio) + '{}' + '.txt')
    file_train = open(path_list.format('train'), 'w')
    file_test = open(path_list.format('test'), 'w')
    path_mask = os.path.join(cfg.root, name_root, '{:02d}', 'rgb')
    for i_type in i_types:
        path_root = path_mask.format(i_type)
        names_img = os.listdir(path_root)
        len_imgs = len(names_img)
        is_test = name_root.startswith('test')
        if cfg.has_cutable_elevs and cut_elevs:
            # to assure equal split
            if is_test:
                len_imgs -= 72 * len([c for c in cut_elevs if c >= 15])
            else:
                len_imgs -= 72 * len(cut_elevs) / (1 if len_imgs > 9 * 72 else 2)
        len_imgs_train = int(len_imgs * train_ratio / 100.0)
        if len_imgs_train % 2 != 0:
            # frcnn needs even number for train
            len_imgs_train -= 1
        i_train = np.random.choice(len_imgs, len_imgs_train, replace=False)
        i = 0  # to assure equal split
        for name_img in sorted(names_img):
            name, ext = os.path.splitext(name_img)
            assert ext.lower() in ['.jpg', '.jpeg', '.png']  # would break choice of i_train
            i_img = int(name)
            if cfg.has_cutable_elevs and cut_elevs:
                if misc.elevation(i_img, is_test) in cut_elevs:
                    continue
            f = file_train if i in i_train else file_test
            f.write(os.path.join(path_root, name_img) + '\n')
            i += 1
    file_train.close()
    file_test.close()


def main():
    args = parse_args()
    generate_list(args.name, args.dir_name, args.id_list, args.train_ratio, args.cut_elevs)


if __name__ == '__main__':
    main()
