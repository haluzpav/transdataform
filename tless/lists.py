import argparse
import os
from glob import glob

import _init_paths
import cfg


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('name')
    parser.add_argument('dir_name')
    parser.add_argument('--id_list', type=int, nargs='+')
    parser.add_argument('--select', type=int, nargs='+')
    parser.add_argument('--base', type=int, default=20)
    parser.add_argument('--cut', type=int)
    return parser.parse_args()


def generate_list(name_list, name_root, range_type, select=None, base=20, cut=None):
    select = select or range(base)
    path_list = os.path.join(cfg.root_lists, name_list + '.txt')
    with open(path_list, 'w') as f:
        path_mask = os.path.join(cfg.root, name_root, '{:02d}', 'rgb', '*.*')
        for i_type in range_type:
            for i_img, path_img in enumerate(sorted(glob(path_mask.format(i_type)))):
                if cut is not None and i_img >= cut:
                    break
                elif i_img % base in select:
                    f.write(path_img + '\n')


# generate_list('train_all', 'train_primesense', range(1, 31))
# generate_list('train_all_every0from20', 'train_primesense', range(1, 31), select=[0])
# generate_list('train_0109', 'train_primesense', [1, 9])
# generate_list('train_0109_every0from20', 'train_primesense', [1, 9], select=[0])
# generate_list('scene_12_upper', 'test_primesense', [12], cut=288)
# generate_list('scene_12_upper_every0from20', 'test_primesense', [12], select=[0], cut=288)


def main():
    args = parse_args()
    if args.id_list is None:
        args.id_list = range(1, 31) if args.dir_name.startswith('train') else range(1, 21)
    generate_list(args.name, args.dir_name, args.id_list, args.select, args.base, args.cut)


if __name__ == '__main__':
    main()
