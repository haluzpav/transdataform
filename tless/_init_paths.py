import os.path
import sys


def add_path(path):
    if path not in sys.path:
        sys.path.insert(0, path)


this_dir = os.path.dirname(__file__)

add_path(os.path.join(this_dir, '..'))


def frcnn_paths():
    add_path(r'/mnt/home.stud/haluzpav/frcnn/caffe-fast-rcnn/python')
    add_path(r'/mnt/home.stud/haluzpav/frcnn/lib')


def import_cfg(dataset='tless'):
    import importlib
    return importlib.import_module(dataset + '.cfg')
