import Queue as queue  # wtf python2
import os
import threading
from glob import glob
import argparse

import cv2
import numpy as np
import yaml

import _init_paths
import cfg
import misc
import bbox_models
import extend_depth
import inpaint
import derivate

sin = cfg.img_sizes['train_primesense']
sout = cfg.img_sizes['train_primesense']
dir_name_src = 'train_primesense'

data = {}
model = None
root_dst = ''


def parse_args(argv=None):
    inpaints = ['median', 'nearest', 'smooth']
    derivates = ['sobel3_absxy']

    parser = argparse.ArgumentParser()
    parser.add_argument('scale', type=float)
    parser.add_argument('angle', type=int)
    parser.add_argument('--rgb', action='store_true')
    parser.add_argument('--depth', action='store_true')
    parser.add_argument('--mask', action='store_true')
    parser.add_argument('--inpaint', choices=inpaints, nargs='+', default=[])
    parser.add_argument('--der_src', choices=inpaints, default='median')
    parser.add_argument('--derivate', choices=derivates, nargs='+', default=[])
    parser.add_argument('--yaml', action='store_true')
    parser.add_argument('--singlethread', action='store_true')
    args = parser.parse_args(argv)

    return args


def rot_mat_z():
    a = np.deg2rad(-args.angle)
    s, c = np.sin(a), np.cos(a)
    return np.matrix([
        [c, -s, 0],
        [s, c, 0],
        [0, 0, 1]
    ])


args = parse_args() if __name__ == '__main__' else parse_args(['1', '0'])
dir_name_dst = '_'.join((dir_name_src, 's{:.2f}'.format(args.scale), 'r{:03d}'.format(args.angle)))
Rz = rot_mat_z()


def crop_image(img):
    n_channels = (img.shape[-1],) if len(img.shape) > 2 else ()
    omg = np.zeros(sout[::-1] + n_channels, dtype=img.dtype)
    tl = ((sout[0] - sin[0]) / 2, (sout[1] - sin[1]) / 2)
    br = (tl[0] + sin[0], tl[1] + sin[1])
    omg[tl[1]:br[1], tl[0]:br[0]] = img
    return omg


def crop_params(K):
    K[0, 2] += (sout[0] - sin[0]) / 2
    K[1, 2] += (sout[1] - sin[1]) / 2


def traffine_image(img, K, interpolation, border=cv2.BORDER_CONSTANT):
    mat = cv2.getRotationMatrix2D((K[0, 2], K[1, 2]), args.angle, args.scale)
    mat[0, 2] += (sout[0] - sin[0]) / 2
    mat[1, 2] += (sout[1] - sin[1]) / 2
    img = cv2.warpAffine(img, mat, sout, flags=interpolation, borderMode=border)
    return img


def traffine_params(K, R, t):
    K[[0, 1], [0, 1]] *= args.scale
    R[:] = Rz.dot(R)
    t[:] = Rz.dot(t)


def job_transform(i, path_rgb_src, path_depth_src):
    global data
    K = np.array(data['info'][i]['cam_K']).reshape((3, -1))
    path_dst = os.path.join(root_dst, '{}', '{:04d}.png'.format(i))

    if args.rgb:
        img_rgb = cv2.imread(path_rgb_src)
        img_rgb = traffine_image(img_rgb, K, cv2.INTER_LINEAR)
        cv2.imwrite(path_dst.format('rgb'), img_rgb)

    if args.depth or args.mask or args.inpaint or args.derivate:
        img_depth = cv2.imread(path_depth_src, cv2.IMREAD_UNCHANGED)
        img_depth = (img_depth / args.scale).astype(img_depth.dtype)

    img_der_src = None

    for inp in args.inpaint:
        method = getattr(inpaint, inp)
        img_inpaint = method(img_depth)
        if img_inpaint is not None:
            # can be None if too much values missing - cut off because of time
            img_inpaint = traffine_image(img_inpaint, K, cv2.INTER_NEAREST, cv2.BORDER_REPLICATE)
            cv2.imwrite(path_dst.format('depth_inpaint_' + inp), img_inpaint)
        if inp == args.der_src:
            img_der_src = img_inpaint

    if img_der_src is None and args.derivate and args.der_src not in args.inpaint:
        # wasn't done during "official" inpainting
        img_inpaint = getattr(inpaint, args.der_src)(img_depth)
        if img_der_src is not None:
            img_der_src = traffine_image(img_inpaint, K, cv2.INTER_NEAREST, cv2.BORDER_REPLICATE)
    if img_der_src is not None:
        for der in args.derivate:
            method = getattr(derivate, der)
            img_derivate = method(img_der_src)
            cv2.imwrite(path_dst.format('depth_' + der), img_derivate)

    if args.depth or args.mask or args.derivate:
        img_depth = traffine_image(img_depth, K, cv2.INTER_NEAREST)
    if args.depth:
        cv2.imwrite(path_dst.format('depth'), img_depth)
    if args.mask:
        img_mask = extend_depth.mask(img_depth)
        cv2.imwrite(path_dst.format('depth_mask'), img_mask)

    if args.yaml:
        crop_params(K)
        R, t = [np.array(data['gt'][i][0][k]).reshape((3, -1)) for k in ('cam_R_m2c', 'cam_t_m2c')]
        traffine_params(K, R, t)
        data['info'][i]['cam_K'] = K.flatten().tolist()
        data['gt'][i][0]['cam_R_m2c'] = R.flatten().tolist()
        data['gt'][i][0]['cam_t_m2c'] = t.flatten().tolist()
        bbox = bbox_models.calc_pose_2d_bbox(model, sout, K, R, t)
        data['gt'][i][0]['obj_bb'] = [int(b) for b in bbox]


def worker(que):
    while True:
        try:
            args_job = que.get(timeout=5)
        except queue.Empty:
            # so the script actually exits (after 5 secs) at the end
            return
        job_transform(*args_job)
        que.task_done()


def transform_train():
    global data, root_dst, model

    print('using args ' + str(args))

    if not args.singlethread:
        que = queue.Queue(20)
        for i in range(4):
            threading.Thread(target=worker, args=(que,)).start()

    root_root_src = os.path.join(cfg.root, dir_name_src)
    for dir_src in os.listdir(root_root_src):

        try:
            int(dir_src)
        except ValueError:
            continue
        root_src = os.path.join(root_root_src, dir_src)

        with open(os.path.join(root_src, 'info.yml')) as f:
            data['info'] = yaml.load(f, yaml.CLoader)
        with open(os.path.join(root_src, 'gt.yml')) as f:
            data['gt'] = yaml.load(f, yaml.CLoader)
        id_obj = os.path.split(root_src)[-1]
        if args.yaml:
            model = bbox_models.load_ply(os.path.join(cfg.root, 'models_cad', 'obj_' + id_obj + '.ply'))

        root_dst = misc.replace_path(root_src, -2, dir_name_dst)

        if args.rgb:
            misc.mkdir(os.path.join(root_dst, 'rgb'))
        if args.depth:
            misc.mkdir(os.path.join(root_dst, 'depth'))
        if args.mask:
            misc.mkdir(os.path.join(root_dst, 'depth_mask'))
        for inp in args.inpaint:
            misc.mkdir(os.path.join(root_dst, 'depth_inpaint_' + inp))
        for der in args.derivate:
            misc.mkdir(os.path.join(root_dst, 'depth_' + der))

        paths_rgb = sorted(glob(os.path.join(root_src, 'rgb', '*.png')))
        paths_depth = sorted(glob(os.path.join(root_src, 'depth', '*.png')))
        assert len(paths_rgb) == len(paths_depth)

        pp = misc.ProgressPrinter(root_dst, 10, len(paths_rgb))
        for i_img, (path_rgb, path_depth) in enumerate(zip(paths_rgb, paths_depth)):
            if args.singlethread:
                job_transform(i_img, path_rgb, path_depth)
            else:
                que.put((i_img, path_rgb, path_depth))
            pp.prog(i_img)
        if not args.singlethread:
            que.join()

        if args.yaml:
            with open(os.path.join(root_dst, 'info.yml'), 'w') as f:
                yaml.dump(data['info'], f, yaml.CDumper)
            with open(os.path.join(root_dst, 'gt.yml'), 'w') as f:
                yaml.dump(data['gt'], f, yaml.CDumper)


if __name__ == '__main__':
    transform_train()
