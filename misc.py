from __future__ import print_function
import os
from scipy.ndimage.filters import uniform_filter1d, gaussian_filter1d
import numpy as np
import sys
import cPickle


def mkdir(path, has_filename=False):
    if has_filename:
        path = os.path.split(path)[0]
    if not os.path.exists(path):
        os.makedirs(path)


def split_path(path):
    split = []
    while path:
        path, rest = os.path.split(path)
        if not rest:
            split.insert(0, path)
            break
        split.insert(0, rest)
    return split


def replace_path(path, i, new):
    split = split_path(path)
    split[i] = new
    return os.path.join(*split)


def moving_average_1d(x, s, kernel='gauss', *args, **kwargs):
    """
    :param x:
    :param s: sigma or size
    :param kernel:
    :return: same size as x
    """
    assert len(x.shape) == 1
    if kernel == 'uniform':
        f = uniform_filter1d
    elif kernel == 'gauss':
        f = gaussian_filter1d
    else:
        raise ValueError('unknown kernel type: {}'.format(kernel))
    return f(x, s, *args, **kwargs)


def moving_average(x, s, kernel='gauss', *args, **kwargs):
    y = np.zeros(x.shape, x.dtype)
    if len(x.shape) == 1:
        return moving_average_1d(x, s, kernel, *args, **kwargs)
    else:
        for i in range(x.shape[1]):
            y[:, i] = moving_average_1d(x[:, i], s, kernel, *args, **kwargs)
        return y


def append_dict(d, key, value):
    if key in d:
        d[key].append(value)
    else:
        d[key] = [value]


def compute_iou(bb_gt, bb_det):
    """
    Compute intersection over union for each pair of ground-truth bounding-boxes and detected bbs.
    :param bb_gt: ground-truth bounding-boxes
    :param bb_det: detected bounding-boxes
    :return: matrix of ious, ious corresponding to one gt-bb are in a row, similarly for detected in a column
    """

    def bb_area(x1, y1, x2, y2):
        return max(0, 1.0 * (x2 - x1) * (y2 - y1))  # intersection could be negative, overflow might happen

    ious = np.zeros((len(bb_gt), len(bb_det)), dtype=float)
    for (i, j), _ in np.ndenumerate(ious):
        a_gt = bb_area(*bb_gt[i])
        a_det = bb_area(*bb_det[j, :4])
        inter = bb_area(
            max(bb_gt[i, 0], bb_det[j, 0]),
            max(bb_gt[i, 1], bb_det[j, 1]),
            min(bb_gt[i, 2], bb_det[j, 2]),
            min(bb_gt[i, 3], bb_det[j, 3]),
        )
        union = a_gt + a_det - inter
        ious[i, j] = inter / union

    return ious


styles_line = ['-', '--', '-.', ':']


def get_style_line(i):
    return styles_line[int(i / 10)]


class ProgressPrinter:
    def __init__(self, name, n_step, len_max):
        self.name = name
        self.n_step = n_step
        self.len_max = len_max
        self.div = int(self.len_max / self.n_step)
        self.mult = 100 / self.n_step
        self._print_name()

    def _print_name(self):
        print(self.name)

    def _percentage(self, i):
        return self.mult * i / self.div

    def prog(self, i):
        if i % self.div == 0:
            print('{:3.0f}%'.format(self._percentage(i)))


class DottedProgressPrinter(ProgressPrinter):
    def __init__(self, name, n_step, len_max):
        ProgressPrinter.__init__(self, name, n_step, len_max)

    def _print_name(self):
        print(self.name, end='')

    def prog(self, i):
        if i % self.div == 0:
            print('.', end='')
            if self._percentage(i) == 100:
                print('')


def prints(*args, **kwargs):
    files = kwargs.pop('file', None)
    if files is None:
        print(*args, **kwargs)
    elif type(files) == str:
        print(*args, file=files, **kwargs)
    else:
        for f in files:
            print(*args, file=f, **kwargs)


def elevation(i, is_test):
    elev = 85 - int(i / 72) * 10
    if is_test:
        # goes from 75 to 15, without negatives
        elev -= 10
    return elev


def load_evals(root):
    path = os.path.join(root, 'evaluation.pkl')
    with open(path, 'rb') as f:
        data = cPickle.load(f)  # , encoding='bytes')  # for python 3
    data = {k.decode("utf-8"): v for k, v in data.items()}
    return data


def latexize_matplotlib():
    import matplotlib
    matplotlib.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
    matplotlib.rc('text', usetex=True)
